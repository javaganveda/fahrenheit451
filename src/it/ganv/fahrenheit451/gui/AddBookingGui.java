package it.ganv.fahrenheit451.gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.sql.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import it.ganv.fahrenheit451.domain.Book;
import it.ganv.fahrenheit451.domain.Booking;
import it.ganv.fahrenheit451.domain.User;
import it.ganv.fahrenheit451.genericfunction.CalendarInterface;
import it.ganv.fahrenheit451.genericfunction.SwingCalendar;
import it.ganv.fahrenheit451.repository.MySqlBookRepository;
import it.ganv.fahrenheit451.repository.MySqlBookingRepository;
import it.ganv.fahrenheit451.repository.MySqlUserRepository;
import it.ganv.fahrenheit451.security.AdminSuccess;

public class AddBookingGui extends JFrame implements CalendarInterface, FocusListener  {

	static AddBookingGui frame;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField1;
	private JTextField textField2;
	private JTextField textField3;
	private JTextField textField4;
	private JTextField textField5;
	private JTextField textField6;
	private JTextField textField7;
	private JTextField textField8;
	JTextField temp;
	boolean open =false;


	private JComboBox listBook;
	private JComboBox listUser;
	CalendarInterface en;
	int contPanel = 0;
	
	SwingCalendar calend,calendTemp;

	DefaultTableModel model;
	Calendar cal = new GregorianCalendar();
	JLabel label;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new AddBookingGui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddBookingGui() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
	//	en = new SwingCalendar(this);

		JLabel lblAddBooking = new JLabel("Add Booking");
		lblAddBooking.setForeground(Color.DARK_GRAY);
		lblAddBooking.setFont(new Font("Tahoma", Font.PLAIN, 22));

		JLabel lblIdBook = new JLabel("ID Book:");
		JLabel lblSearch = new JLabel("");

		JLabel lblIdUser = new JLabel("ID User:");

		JLabel lblReservationDate = new JLabel("Reservation Date:");

		JLabel lblTakingDate = new JLabel("Taking Date");

		JLabel lblDeliveryDate = new JLabel("Delivery Date");

		JLabel lblTaken = new JLabel("Taken?");

		JLabel lblDelivered = new JLabel("Delivered?");


		listBook = new JComboBox();
		//listBook.setColumns(20);
		for(Book book : MySqlBookRepository.getAll()) {
			listBook.addItem(book.getTitle());
		}
		listBook.setEditable(false);



		listUser = new JComboBox();
		//textField1.setColumns(20);
		for(User user : MySqlUserRepository.getAll()) {
			listUser.addItem(user.getSurname());
		}
		listUser.setEditable(false);

		textField2 = new JTextField();
		textField2.addFocusListener(this);
		textField2.setColumns(20);


		textField3 = new JTextField();
		textField3.addFocusListener(this);
		textField3.setColumns(20);
		
		/* textField3.addFocusListener(new FocusListener() {
			public void focusGained(FocusEvent e) {
				PointerInfo a = MouseInfo.getPointerInfo();
				calend.openCalendar(a.getLocation().x,a.getLocation().y, null);
			}

			public void focusLost(FocusEvent e) {
				// displayMessage("Focus lost", e);
			}

		}); */

		textField4 = new JTextField();
		AddBookingGui t = this;
		
		textField4.addFocusListener(this);
		textField4.setColumns(20);

		textField5 = new JTextField();
		textField5.setColumns(20);

		textField6 = new JTextField();
		textField6.setColumns(20);

		textField7 = new JTextField();
		textField7.addFocusListener(this);
		textField7.setColumns(20);

		textField8 = new JTextField();
		textField8.addFocusListener(this);
		textField8.setColumns(20);

		JButton btnNewButton = new JButton("Add Booking");

		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Booking booking = new Booking();


				booking.setIdBook(Integer.parseInt(textField.getText()));
				booking.setIdUser(Integer.parseInt(textField1.getText()));
				booking.setReservationDate(Date.valueOf(textField2.getText()));
				booking.setTakingDate(Date.valueOf(textField3.getText()));
				booking.setDeliveryDate(Date.valueOf(textField4.getText()));
				booking.setTaken(Boolean.valueOf(textField5.getText()));
				booking.setDelivered(Boolean.valueOf(textField6.getText()));


				MySqlBookingRepository.addBooking(booking);

				int i = 1;

				if(i>0){
					JOptionPane.showMessageDialog(AddBookingGui.this,"Booking added successfully!");
					AdminSuccess.main(new String[]{});
					frame.dispose();

				}else{
					JOptionPane.showMessageDialog(AddBookingGui.this,"Sorry, unable to save!");
				}


			}
		});
		btnNewButton.setForeground(Color.DARK_GRAY);

		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdminSuccess.main(new String[]{});
				frame.dispose();
			}
		});

		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
				gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
						.addGap(20)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
								.addComponent(lblIdBook, GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE)
								.addComponent(lblSearch, GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE)
								.addComponent(lblIdUser, GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE)
								.addComponent(lblSearch, GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE)
								.addComponent(lblReservationDate, GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE)
								.addComponent(lblTakingDate, GroupLayout.DEFAULT_SIZE, 62, GroupLayout.DEFAULT_SIZE)
								.addComponent(lblDeliveryDate, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(lblTaken, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.DEFAULT_SIZE)
								.addComponent(lblDelivered, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))

						.addGap(58)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
								.addComponent(textField8, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
								.addComponent(textField7, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
								.addComponent(textField6, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
								.addComponent(textField5, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
								.addComponent(textField4, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
								.addComponent(textField3, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
								.addComponent(textField2, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
								.addComponent(listUser, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
								.addComponent(listBook, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE))
						.addContainerGap(107, Short.MAX_VALUE))
				.addGroup(gl_contentPane.createSequentialGroup()
						.addContainerGap(151, Short.MAX_VALUE)
						.addComponent(lblAddBooking)
						.addGap(144))
				.addGroup(gl_contentPane.createSequentialGroup()
						.addContainerGap(160, Short.MAX_VALUE)
						.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 131, GroupLayout.PREFERRED_SIZE)
						.addGap(133))
				.addGroup(gl_contentPane.createSequentialGroup()
						.addContainerGap(200, Short.MAX_VALUE)
						.addComponent(btnBack)
						.addGap(169))
				);
		gl_contentPane.setVerticalGroup(
				gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
						.addComponent(lblAddBooking)
						.addGap(18)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPane.createSequentialGroup()
										.addComponent(lblIdBook)
										.addGap(18)
										.addComponent(lblSearch)
										.addGap(18)
										.addComponent(lblIdUser)
										.addGap(18)
										.addComponent(lblSearch))
								.addGroup(gl_contentPane.createSequentialGroup()
										.addComponent(listBook, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addComponent(textField7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addComponent(listUser, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addComponent(textField8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)

										))
						.addGap(18)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblReservationDate)
								.addComponent(textField2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(18)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblTakingDate)
								.addComponent(textField3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(18)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblDeliveryDate)
								.addComponent(textField4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(18)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblTaken)
								.addComponent(textField5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(18)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblDelivered)
								.addComponent(textField6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(18)


						.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED, 57, Short.MAX_VALUE)
						.addComponent(btnBack)
						.addGap(19))
				);
		contentPane.setLayout(gl_contentPane);
	}





	@Override
	public void handleInsertedDate(String data, SwingCalendar panelGlobal) {
		// TODO Auto-generated method stub
		
		temp.setText(data);
		//contPanel=1;
		calendTemp=panelGlobal;
		panelGlobal.setVisible(false);
	//	panelGlobal = null;
		temp=null;
	}

	@Override
	public void focusGained(FocusEvent e) {
		
		contPanel++;
		temp = (JTextField) e.getComponent();

		if(contPanel<=1 && temp !=null){
		PointerInfo a = MouseInfo.getPointerInfo();
		calend = new SwingCalendar(a.getLocation().x,a.getLocation().y,this);
		calend.openCalendar(this);
		contPanel=0;
		open = true;

		}
		
	}

	@Override
	public void focusLost(FocusEvent e) {
		// TODO Auto-generated method stub
		
	}

	


}
