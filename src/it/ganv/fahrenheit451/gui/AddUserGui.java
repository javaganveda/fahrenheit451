package it.ganv.fahrenheit451.gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import it.ganv.fahrenheit451.domain.User;
import it.ganv.fahrenheit451.repository.MySqlUserRepository;
import it.ganv.fahrenheit451.security.AdminSuccess;

public class AddUserGui  extends JFrame {
	static AddUserGui frame;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField1;
	private JTextField textField2;
	private JTextField textField3;
	private JTextField textField4;
	private JTextField textField5;
	private JTextField textField6;
	private JTextField textField7;
	private JTextField textField8;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new AddUserGui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddUserGui() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 200, 700, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		JLabel lblAddUser = new JLabel("Add User");
		lblAddUser.setForeground(Color.DARK_GRAY);
		lblAddUser.setFont(new Font("Tahoma", Font.PLAIN, 30));
		

		JLabel lblName = new JLabel("Name:");
		
		JLabel lblSurname = new JLabel("Surname:");
		
		JLabel lblAddress = new JLabel("Address:");
		
		JLabel lblTelephone = new JLabel("Telephone:");
		
		JLabel lblMail = new JLabel("Mail:");
		
		JLabel lblUsername = new JLabel("Username:");
		
		JLabel lblPassword = new JLabel("Password:");
		
		JLabel lblTipology = new JLabel("Tipology:");
		
		JLabel lblCF = new JLabel("CF:");
		
		
		
		textField = new JTextField();
		textField.setColumns(20);
		
		textField1 = new JTextField();
		textField1.setColumns(20);
		
		textField2 = new JTextField();
		textField2.setColumns(20);
		
		textField3 = new JTextField();
		textField3.setColumns(20);
		
		textField4 = new JTextField();
		textField4.setColumns(20);
		
		textField5 = new JTextField();
		textField5.setColumns(20);
		
		textField6 = new JTextField();
		textField6.setColumns(20);
		
		textField7 = new JTextField();
		textField7.setColumns(20);
		
		textField8 = new JTextField();
		textField8.setColumns(20);
		
		JButton btnNewButton = new JButton("Add User");
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
		
			User user = new User();
			
			//book.setTitle(textField.getText());
			user.setName(textField.getText());
			user.setSurname(textField1.getText());
			user.setAddress(textField2.getText());
			user.setTelephone(Integer.parseInt(textField3.getText()));
			user.setMail(textField4.getText());
			user.setUsername(textField5.getText());
			user.setPassword(textField6.getText());
			user.setTipology(Integer.parseInt(textField7.getText()));
			user.setCF(textField8.getText());

			
			MySqlUserRepository.addUser(user);
			int i = 1;
			
			if(i>0){
				JOptionPane.showMessageDialog(AddUserGui.this,"User added successfully!");
				AdminSuccess.main(new String[]{});
				frame.dispose();
				
			}else{
				JOptionPane.showMessageDialog(AddUserGui.this,"Sorry, unable to save!");
			}
			
			
			}
		});
		btnNewButton.setForeground(Color.DARK_GRAY);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			AdminSuccess.main(new String[]{});
			frame.dispose();
			}
		});
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(20)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
						.addComponent(lblName, GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE)
						.addComponent(lblSurname, GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE)
						.addComponent(lblAddress, GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE)
						.addComponent(lblTelephone, GroupLayout.DEFAULT_SIZE, 62, GroupLayout.DEFAULT_SIZE)
						.addComponent(lblMail, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(lblUsername, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.DEFAULT_SIZE)
						.addComponent(lblPassword, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(lblTipology, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.DEFAULT_SIZE)
						.addComponent(lblCF, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
						
					.addGap(58)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
							.addComponent(textField8, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
							.addComponent(textField7, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
							.addComponent(textField6, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
							.addComponent(textField5, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
							.addComponent(textField4, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
							.addComponent(textField3, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
							.addComponent(textField2, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
							.addComponent(textField1, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
							.addComponent(textField, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE))
					.addContainerGap(107, Short.MAX_VALUE))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap(151, Short.MAX_VALUE)
					.addComponent(lblAddUser)
					.addGap(144))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap(160, Short.MAX_VALUE)
					.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 131, GroupLayout.PREFERRED_SIZE)
					.addGap(133))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap(200, Short.MAX_VALUE)
					.addComponent(btnBack)
					.addGap(169))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(lblAddUser)
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(lblName)
							.addGap(18)
							.addComponent(lblSurname))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(textField1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblAddress)
						.addComponent(textField2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblTelephone)
						.addComponent(textField3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblMail)
						.addComponent(textField4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblUsername)
						.addComponent(textField5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblPassword)
						.addComponent(textField6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblTipology)
						.addComponent(textField7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)	
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblCF)
						.addComponent(textField8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					
					
					.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED, 57, Short.MAX_VALUE)
						.addComponent(btnBack)
					.addGap(19))
		);
		
		contentPane.setLayout(gl_contentPane);
	}

}
