package it.ganv.fahrenheit451.gui;

import java.awt.Color;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import it.ganv.fahrenheit451.security.AdminLogin;
import it.ganv.fahrenheit451.security.UserLogin;

public class GraphicMenu extends JFrame {
		static GraphicMenu frame;
		private JPanel contentPane;

		/**
		 * Launch the application.
		 */
		
		public static void main(String[] args) {
			EventQueue.invokeLater(new Runnable() {
				
				public void run() {
					try {
						frame = new GraphicMenu();
						frame.setVisible(true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}//fine RUN
			});
		}

		/**
		 * Create the frame.
		 */
		public GraphicMenu() {
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setBounds(100, 100, 450, 300);
			contentPane = new JPanel();
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			setContentPane(contentPane);
			
			//Label della finestra
			JLabel lblLibraryManagement = new JLabel("Fahrenheit451 - GRUPPO GANV");
			lblLibraryManagement.setFont(new Font("Tahoma", Font.PLAIN, 18));
			lblLibraryManagement.setForeground(Color.GRAY);
			
			//Inserire button
			JButton btnAdminLogin = new JButton("Admin Login");
			
			//associao il metodo al pulsante
			btnAdminLogin.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
				AdminLogin.main(new String[]{});
				frame.dispose();//dispose la finestra di main
				}
			});
			///fine button Admin Login

			btnAdminLogin.setFont(new Font("Tahoma", Font.PLAIN, 15));
			
			JButton btnLibrarianLogin = new JButton("User Login");
			btnLibrarianLogin.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					UserLogin.main(new String[]{});
				}
			});
			
			btnLibrarianLogin.setFont(new Font("Tahoma", Font.PLAIN, 15));
			///fine button user Login
			
			
			//Posizionamento dei button creati
			GroupLayout gl_contentPane = new GroupLayout(contentPane);
			gl_contentPane.setHorizontalGroup(
				gl_contentPane.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_contentPane.createSequentialGroup()
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_contentPane.createSequentialGroup()
								.addGap(64)
								.addComponent(lblLibraryManagement))
							.addGroup(gl_contentPane.createSequentialGroup()
								.addGap(140)
								.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
									.addComponent(btnLibrarianLogin, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(btnAdminLogin, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE))))
						.addContainerGap(95, Short.MAX_VALUE))
			);
			gl_contentPane.setVerticalGroup(
				gl_contentPane.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_contentPane.createSequentialGroup()
						.addContainerGap()
						.addComponent(lblLibraryManagement)
						.addGap(32)
						.addComponent(btnAdminLogin, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(btnLibrarianLogin, GroupLayout.PREFERRED_SIZE, 53, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(70, Short.MAX_VALUE))
			);
			contentPane.setLayout(gl_contentPane);
		}
	}
