package it.ganv.fahrenheit451.gui;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import it.ganv.fahrenheit451.Setting;
import it.ganv.fahrenheit451.domain.Book;
import it.ganv.fahrenheit451.domain.Publisher;
import it.ganv.fahrenheit451.domain.User;
import it.ganv.fahrenheit451.genericfunction.InputFunction;
import it.ganv.fahrenheit451.genericfunction.SearchFunction;
import it.ganv.fahrenheit451.iodata.BookReader;
import it.ganv.fahrenheit451.iodata.BookWriter;
import it.ganv.fahrenheit451.repository.MySqlAuthorRepository;
import it.ganv.fahrenheit451.repository.MySqlBookRepository;
import it.ganv.fahrenheit451.repository.MySqlBookingRepository;
import it.ganv.fahrenheit451.repository.MySqlPublisherRepository;
import it.ganv.fahrenheit451.repository.MySqlUserRepository;
import it.ganv.fahrenheit451.repository.basicFunctionSql;
import it.ganv.fahrenheit451.security.LoginRegister;

public class TextualMenu {

	public static void startMenu(){

			/* crea l�oggetto che rappresenta la tastiera */	
			System.out.println("|-------------------------------|"); //istruzione per stampare a video 
			System.out.println("|------------Men�---------------|");
			System.out.println("|-------------------------------|"); //istruzione per stampare a video 
			System.out.println("|1 - (L)ogin/Register           |");
			System.out.println("|2 - (B)ooking                  |");
			System.out.println("|3 - (U)ser                     |");
			System.out.println("|4 - Boo(k)                     |");
			System.out.println("|5 - (A)uthor                   |");
			System.out.println("|6 - (P)ublisher                |");
			System.out.println("|7 - (O)ptions                  |");
			System.out.println("|0 - (E)xit                     |");
			System.out.println("|-------------------------------|"); //istruzione per stampare a video 
			System.out.println("|-------------------------------|"); //istruzione per stampare a video 
			
		
		Scanner keyPressed = new Scanner(System.in);
		int keyInt = keyPressed.nextInt();
		keyPressed.close();
		
		switch(keyInt)
		{
		case 1:
			if(Setting.administrator == true)
			{
				loginRegisterMenuAdministrator();	
				Setting.simpleUser= false;
				Setting.registerUser = false;
				Setting.administrator = true;
			}
			else if(Setting.registerUser == true){
				Setting.simpleUser= false;
				Setting.administrator = false;
				Setting.registerUser = true;
				loginRegisterMenuAdministrator();
			}
			else{
				Setting.registerUser = false;
				Setting.administrator = false;
				Setting.simpleUser= true;
				loginRegisterMenuAdministrator();
			}
			
			break;

		case 2:
			if(Setting.administrator == true)
			{
				
				bookingMenuAdministrator();
				Setting.simpleUser= false;
				Setting.registerUser = false;
				Setting.administrator = true;
			}
			else if(Setting.registerUser == true){
				Setting.simpleUser= false;
				Setting.administrator = false;
				Setting.registerUser = true;
				//bookingMenuUser();
			}
			else{
				Setting.registerUser = false;
				Setting.administrator = false;
				Setting.simpleUser= true;
				//bookingMenuSimpleUser();
			}
			
			
		break;
		
		case 3:
			if(Setting.administrator == true)
			{
				userMenuAdministrator();
				Setting.simpleUser= false;
				Setting.registerUser = false;
				Setting.administrator = true;
			}
			else if(Setting.registerUser == true){
				Setting.simpleUser= false;
				Setting.administrator = false;
				Setting.registerUser = true;
				System.out.println("Non hai i privilegi necessari");
				//userMenuUser();
			}
			else{
				Setting.registerUser = false;
				Setting.administrator = false;
				Setting.simpleUser= true;
				System.out.println("Non hai i privilegi necessari");
			}
		break;
		
		case 4:
			
			if(Setting.administrator == true)
			{
				bookMenuAdministrator();	
				Setting.simpleUser= false;
				Setting.registerUser = false;
				Setting.administrator = true;
			}
			else if(Setting.registerUser == true){
				Setting.simpleUser= false;
				Setting.administrator = false;
				Setting.registerUser = true;
				bookMenuUser();
			}
			else{
				Setting.registerUser = false;
				Setting.administrator = false;
				Setting.simpleUser= true;
				bookMenuUser();
			}
					
			break;
			
		case 5:
			if(Setting.administrator == true)
			{
				authorMenuAdministrator();	
				Setting.simpleUser= false;
				Setting.registerUser = false;
				Setting.administrator = true;
			}
			else if(Setting.registerUser == true){
				Setting.simpleUser= false;
				Setting.administrator = false;
				Setting.registerUser = true;
				//authorMenuRegister();
				}
			else{
				Setting.registerUser = false;
				Setting.administrator = false;
				Setting.simpleUser= true;
				//authorMenuSimple();
				}
			
			break;
			
		case 6:
			if(Setting.administrator == true)
			{
				publisherMenuAdministrator();	
				Setting.simpleUser= false;
				Setting.registerUser = false;
				Setting.administrator = true;
			}
			else if(Setting.registerUser == true){
				Setting.simpleUser= false;
				Setting.administrator = false;
				Setting.registerUser = true;
				//publisherMenuRegister();
			}
			else{
				Setting.registerUser = false;
				Setting.administrator = false;
				Setting.simpleUser= true;
				//publisherMenuSimple();
			}
						break;
			
		case 7:
			if(Setting.administrator == true)
			{
				optionAdministrator();	
				Setting.simpleUser= false;
				Setting.registerUser = false;
				Setting.administrator = true;
			}
			else if(Setting.registerUser == true){
				Setting.simpleUser= false;
				Setting.administrator = false;
				Setting.registerUser = true;
				System.out.println("Non hai i privilegi necessari");
			}
			else{
				Setting.registerUser = false;
				Setting.administrator = false;
				Setting.simpleUser= true;
				System.out.println("Non hai i privilegi necessari");
			}
			
			break;
			
		case 0:
		System.exit(1);
		break;
		
		default:
		System.out.println("Comando non trovato");
		System.out.println("");
		break;
		} 	
		
	}//FINE METODO
	
static public void optionAdministrator(){
		
		try (Scanner keyPressed = new Scanner(System.in)){

			
		do{
			System.out.println("|----------------------------|"); //istruzione per stampare a video 
			System.out.println("|------------Options---------|");
			System.out.println("|----------------------------|"); //istruzione per stampare a video 
			System.out.println("|1 - Create Scheme           |");
			System.out.println("|2 - Create Table            |");
			System.out.println("|3 - Delete Scheme           |");
			System.out.println("|4 - Delete Table            |");
			System.out.println("|5 - Import From file        |");
			System.out.println("|6 - Export to   file        |");
			System.out.println("|0 - back                    |");
			System.out.println("|----------------------------|");		
		
			int keyInt = keyPressed.nextInt();

			
		switch(keyInt){
		
		case 1:
			
				MySqlBookRepository.createSchema();
				
				System.out.println("Press Any Key To Continue...");
		          new java.util.Scanner(System.in).nextLine();
			
		          optionAdministrator();
			break;
		
		case 2:
	
			basicFunctionSql.createBookTable();
			basicFunctionSql.createAuthorTable();
			basicFunctionSql.createPublisherTable();
			basicFunctionSql.createUserTable();
			basicFunctionSql.createBookingTable();
			
			System.out.println("Press Any Key To Continue...");
	          new java.util.Scanner(System.in).nextLine();
	          
	          optionAdministrator();

			break;	
			
		case 3:
			
		

			break;
		
		case 4:
	

			break;
			
		case 5:
///////////////// LEGGI Book

					for(Book books : MySqlBookRepository.getAll()){
					MySqlBookRepository.addBook(books);
					}
				
			break;
		
		case 6:
			//////////////////SCRIVI Book

			BookWriter.saveBook(MySqlBookRepository.getAll());
			break;	
			
		case 0:
			startMenu();
			break;
	
		default:
			System.out.println("Choose correct option...");
			break;
		}	
	}while(keyPressed.hasNextInt() || keyPressed.hasNextLine()); }	
	}
	


static public void bookMenuAdministrator(){
	
	try (Scanner keyPressed = new Scanner(System.in)){

		
	do{
		System.out.println("|----------------------------|"); //istruzione per stampare a video 
		System.out.println("|------------Book------------|");
		System.out.println("|----------------------------|"); //istruzione per stampare a video 
		System.out.println("|1 - View List Book          |");
		System.out.println("|2 - Search Book             |");
		System.out.println("|3 - Insert Book             |");
		System.out.println("|4 - Update Book             |");
		System.out.println("|5 - Delete Book             |");
		System.out.println("|0 - back                    |");
		System.out.println("|----------------------------|");		
	
		int keyInt = keyPressed.nextInt();

		
	switch(keyInt){
	
	case 1:
		
		SearchFunction.chooseIdBook();
	
			System.out.println("Press Any Key To Continue...");
	          new java.util.Scanner(System.in).nextLine();
		
	          bookMenuAdministrator();
		break;
	
	case 2:

		SearchFunction.searchBook();

		
          bookMenuAdministrator();
		break;	
		
	case 3:
		MySqlBookRepository.addBook(InputFunction.insertBook());

	
		
        bookMenuAdministrator();
		break;
	
	case 4:
		try (Scanner keyPressedInt = new Scanner(System.in)){

		int idbook = 0;

		SearchFunction.chooseIdBook();
		
		idbook = keyPressedInt.nextInt();

		MySqlBookRepository.updateBook(MySqlBookRepository.getById(idbook),idbook);
		
		
        bookMenuAdministrator();
		}
		break;
		
	case 5:

		try (Scanner keyPressedInt = new Scanner(System.in)){

		int idbook = 0;

		SearchFunction.chooseIdBook();
		
		idbook = keyPressedInt.nextInt();
		
		MySqlBookRepository.deleteBook(idbook);
		
		
        bookMenuAdministrator();
		break;
		}
	case 0:
		startMenu();
		break;

	default:
		System.out.println("Choose correct option...");
		break;
	}	
}while(keyPressed.hasNextInt() || keyPressed.hasNextLine()); }	
}

static public void bookMenuUser(){
	
	try (Scanner keyPressed = new Scanner(System.in)){

		
	do{
		System.out.println("|----------------------------|"); //istruzione per stampare a video 
		System.out.println("|------------Book------------|");
		System.out.println("|----------------------------|"); //istruzione per stampare a video 
		System.out.println("|1 - View List Book          |");
		System.out.println("|2 - Search Book             |");
		System.out.println("|0 - back                    |");
		System.out.println("|----------------------------|");		
	
		int keyInt = keyPressed.nextInt();

		
	switch(keyInt){
	
	case 1:
		
		SearchFunction.chooseIdBook();

			//MySqlBookRepository.getAllId();
			
			
	          bookMenuUser();
		break;
	
	case 2:

		SearchFunction.searchBook();
		
		   
          bookMenuUser();
		break;	
		
	case 0:
		startMenu();
		break;

	default:
		System.out.println("Choose correct option...");
		break;
	}	
}while(keyPressed.hasNextInt() || keyPressed.hasNextLine()); }	
}

static public void loginRegisterMenuAdministrator() {
	
	

	try (Scanner keyPressed = new Scanner(System.in);){
	
		int keyInt;
		
	do{
		

		System.out.println("|----------------------------|"); //istruzione per stampare a video 
		System.out.println("|------Login/register--------|");
		System.out.println("|----------------------------|"); //istruzione per stampare a video 
		System.out.println("|1 - Login                   |");
		System.out.println("|2 - Register                |");
		System.out.println("|0 - back                    |");
		System.out.println("|----------------------------|");		
		
//		
		keyInt = keyPressed.nextInt();

		
	switch(keyInt){
	
	case 1:
		
		LoginRegister.Login();

			System.out.println("Press Any Key To Continue...");
		
			loginRegisterMenuAdministrator();
			
		
		break;
	
	case 2:

		LoginRegister.Register();

	
          loginRegisterMenuAdministrator();
		break;	
		
	case 0:
		startMenu();
		break;

	default:
		System.out.println("Choose correct option...");
		break;
	}	
}while(keyPressed.hasNextInt() || keyPressed.hasNextLine());
		}	
}


static public void bookingMenuAdministrator(){
	
	try (Scanner keyPressed = new Scanner(System.in)){

		
	do{
		System.out.println("|----------------------------|"); //istruzione per stampare a video 
		System.out.println("|-----------Booking----------|");
		System.out.println("|----------------------------|"); //istruzione per stampare a video 
		System.out.println("|1 - View Booking            |");
		System.out.println("|2 - Search Booking          |");
		System.out.println("|3 - Insert Booking          |");
		System.out.println("|4 - Update Booking          |");
		System.out.println("|5 - Delete Booking          |");
		System.out.println("|0 - back                    |");
		System.out.println("|----------------------------|");		
	
		try {
		int keyInt = keyPressed.nextInt();
		
		 
	switch(keyInt){
	
	case 1:
		
		SearchFunction.chooseIdBooking();

		        
		          bookingMenuAdministrator();
		break;
	
	case 2:

		SearchFunction.searchBooking();
		 
			
				bookingMenuAdministrator();
		break;	
		
	case 3:
		
		MySqlBookingRepository.addBooking(InputFunction.insertBooking());

			
				bookingMenuAdministrator();
		break;
		
	case 4:

		try (Scanner keyPressedInt = new Scanner(System.in)){

			int idbooking = 0;

			SearchFunction.chooseIdBooking();
			
			idbooking = keyPressedInt.nextInt();

			MySqlBookingRepository.updateBooking(MySqlBookingRepository.getById(idbooking),idbooking);
		   bookingMenuAdministrator();
			}
		break;
		
	case 5:

		try (Scanner keyPressedInt = new Scanner(System.in)){

			int idbooking = 0;

			SearchFunction.chooseIdBooking();
			
			idbooking = keyPressedInt.nextInt();
			
			MySqlBookingRepository.deleteBooking(idbooking);
			
		    bookingMenuAdministrator();
		break;
		}
	case 0:
		startMenu();
		break;

	default:
		System.out.println("Choose correct option...");
		break;
		
	}	
	
		}
		   catch (Exception exc) {
			     
		    }
		
}while(keyPressed.hasNextInt() || keyPressed.hasNextLine()); }	
}


static public void userMenuAdministrator(){
	
	
		System.out.println("|----------------------------|"); //istruzione per stampare a video 
		System.out.println("|------------User------------|");
		System.out.println("|----------------------------|"); //istruzione per stampare a video 
		System.out.println("|1 - View User               |");
		System.out.println("|2 - Search User             |");
		System.out.println("|3 - Insert User             |");
		System.out.println("|4 - Update User             |");
		System.out.println("|5 - Delete User             |");
		System.out.println("|0 - back                    |");
		System.out.println("|----------------------------|");		
	
		int keyInt = 0;
		
		Scanner keyPressed = new Scanner(System.in);
	
			keyInt = keyPressed.nextInt();
		
		
	switch(keyInt){
	
	case 1:
		
		SearchFunction.chooseIdUser();

			//MySqlBookingRepository.getAllId();
		          
		          userMenuAdministrator();
		break;
	
	case 2:

		SearchFunction.searchUser();
		 
			
				userMenuAdministrator();
		break;	
		
	case 3:
		
		MySqlUserRepository.addUser(InputFunction.insertUser());

		 
			
				//userMenuAdministrator();
		break;
		
	case 4:

		try (Scanner keyPressedInt = new Scanner(System.in)){

			int iduser = 0;

			SearchFunction.chooseIdUser();
			
			iduser = keyPressedInt.nextInt();

			MySqlUserRepository.updateUser(MySqlUserRepository.getById(iduser),iduser);
			
		      userMenuAdministrator();
			}
		break;
		
	case 5:

		try (Scanner keyPressedInt = new Scanner(System.in)){

			int idbooking = 0;

			SearchFunction.chooseIdBooking();
			
			idbooking = keyPressedInt.nextInt();
			
			MySqlBookingRepository.deleteBooking(idbooking);
			
		     userMenuAdministrator();
		break;
		}
	case 0:
		startMenu();
		break;

	default:
		System.out.println("Choose correct option...");
		break;
		
	}	
	keyPressed.close();		

	}
		
	



static public void authorMenuAdministrator(){
	
	try (Scanner keyPressed = new Scanner(System.in)){

		
	do{
		System.out.println("|----------------------------|"); //istruzione per stampare a video 
		System.out.println("|------------Author----------|");
		System.out.println("|----------------------------|"); //istruzione per stampare a video 
		System.out.println("|1 - View Author             |");
		System.out.println("|2 - Search Author           |");
		System.out.println("|3 - Insert Author           |");
		System.out.println("|4 - Update Author           |");
		System.out.println("|5 - Delete Author           |");
		System.out.println("|0 - back                    |");
		System.out.println("|----------------------------|");		
	
		try {
		int keyInt = keyPressed.nextInt();
		
		 
	switch(keyInt){
	
	case 1:
		
		SearchFunction.chooseIdAuthor();

			//MySqlBookingRepository.getAllId();
		          
		          authorMenuAdministrator();
		break;
	
	case 2:

		SearchFunction.searchAuthor();
		 
				authorMenuAdministrator();
		break;	
		
	case 3:
		
		MySqlAuthorRepository.addAuthor(InputFunction.insertAuthor());

		 
				authorMenuAdministrator();
		break;
		
	case 4:

		try (Scanner keyPressedInt = new Scanner(System.in)){

			int idauthor = 0;

			SearchFunction.chooseIdUser();
			
			idauthor = keyPressedInt.nextInt();

			MySqlAuthorRepository.updateAuthor(MySqlAuthorRepository.getById(idauthor),idauthor);
			
		    authorMenuAdministrator();
			}
		break;
		
	case 5:

		try (Scanner keyPressedInt = new Scanner(System.in)){

			int idauthor = 0;

			SearchFunction.chooseIdAuthor();
			
			idauthor = keyPressedInt.nextInt();
			
			MySqlAuthorRepository.deleteAuthor(idauthor);
		    authorMenuAdministrator();
		break;
		}
	case 0:
		startMenu();
		break;

	default:
		System.out.println("Choose correct option...");
		break;
		
	}	
	
		}
		   catch (Exception exc) {
			     
		    }
		
}while(keyPressed.hasNextInt() || keyPressed.hasNextLine()); }	
}

static public void publisherMenuAdministrator(){
	
	try (Scanner keyPressed = new Scanner(System.in)){

		
	do{
		System.out.println("|----------------------------|"); //istruzione per stampare a video 
		System.out.println("|-----------Publisher--------|");
		System.out.println("|----------------------------|"); //istruzione per stampare a video 
		System.out.println("|1 - View Publisher             |");
		System.out.println("|2 - Search Publisher           |");
		System.out.println("|3 - Insert Publisher           |");
		System.out.println("|4 - Update Publisher           |");
		System.out.println("|5 - Delete Publisher           |");
		System.out.println("|0 - back                    |");
		System.out.println("|----------------------------|");		
	
		try {
		int keyInt = keyPressed.nextInt();
		
		 
	switch(keyInt){
	
	case 1:
		
		SearchFunction.chooseIdPublisher();

			//MySqlBookingRepository.getAllId();
			
		         
		          publisherMenuAdministrator();
		break;
	
	case 2:

		SearchFunction.searchPublisher();
		 
			
				publisherMenuAdministrator();
		break;	
		
	case 3:
		
		MySqlPublisherRepository.addPublisher(InputFunction.insertPublisher());

		 
			
				publisherMenuAdministrator();
		break;
		
	case 4:

		try (Scanner keyPressedInt = new Scanner(System.in)){

			int idpublisher = 0;

			SearchFunction.chooseIdPublisher();
			
			idpublisher = keyPressedInt.nextInt();

			MySqlPublisherRepository.updatePublisher(MySqlPublisherRepository.getById(idpublisher),idpublisher);
			
		   publisherMenuAdministrator();
			}
		break;
		
	case 5:

		try (Scanner keyPressedInt = new Scanner(System.in)){

			int idpublisher = 0;

			SearchFunction.chooseIdPublisher();
			
			idpublisher = keyPressedInt.nextInt();
			
			MySqlPublisherRepository.deletePublisher(idpublisher);
			
		   publisherMenuAdministrator();
		break;
		}
	case 0:
		startMenu();
		break;

	default:
		System.out.println("Choose correct option...");
		break;
		
	}	
	
		}
		   catch (Exception exc) {
			     
		    }
		
}while(keyPressed.hasNextInt() || keyPressed.hasNextLine()); }	
}


	


	


}
