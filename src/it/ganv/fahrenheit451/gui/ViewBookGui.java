package it.ganv.fahrenheit451.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import it.ganv.fahrenheit451.domain.Author;
import it.ganv.fahrenheit451.domain.Book;
import it.ganv.fahrenheit451.genericfunction.ControlFunction;
import it.ganv.fahrenheit451.repository.MySqlAuthorRepository;
import it.ganv.fahrenheit451.repository.MySqlBookRepository;
import it.ganv.fahrenheit451.security.AdminSuccess;

public class ViewBookGui extends JFrame {
	static ViewBookGui frame;
	private JPanel contentPane;
	private JComboBox listIdBook;
	private JTextField textField1;
	private JTextField textField2;
	private JTextField textField3;
	private JTextField textField4;
	private JTextField textField5;
	private JTextField textField6;
	private JTextField textField7;
	private JTextField textField8;
	private JTextField textField9;
	private JTextField textField10;
	private JTextField textField11;
	private JTextField textSearch;
	Book bookChoose;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new ViewBookGui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ViewBookGui() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblAddBook = new JLabel("View Book");
		lblAddBook.setForeground(Color.DARK_GRAY);
		lblAddBook.setFont(new Font("Tahoma", Font.PLAIN, 22));
		
		JLabel lblIdBook = new JLabel("ID Book:");
		JLabel lblTitle = new JLabel("Title:");
		JLabel lblISBN = new JLabel("ISBN:");
		JLabel lblGenre = new JLabel("Genre:");
		JLabel lblIdPublisher = new JLabel("Id Publisher:");
		JLabel lblSummary = new JLabel("Summary:");
		JLabel lblLanguage = new JLabel("Language:");
		JLabel lblEdition = new JLabel("Edition:");
		JLabel lblPublicationDate = new JLabel("Publication Date:");
		JLabel lblIdWriter = new JLabel("Id Writer:");
		JLabel lblSearch = new JLabel("Search");
		JLabel lblQuantity = new JLabel("Quantity:");
		JLabel lblPosition = new JLabel("Position:");

		listIdBook = new JComboBox();
		for(Book book : MySqlBookRepository.getAll()) {
			listIdBook.addItem(book.getIdBook());
		}
		listIdBook.setEditable(false);
		listIdBook.addActionListener(new ActionListener() {//add actionlistner to listen for change
		    @Override
		    public void actionPerformed(ActionEvent e) {

		        int id = (int) listIdBook.getSelectedItem();//get the selected item

		        
		        bookChoose = MySqlBookRepository.getById(id);
		        
		       //compiliamo libro
		        
		        textField1.setText(bookChoose.getTitle());
		        textField2.setText(bookChoose.getISBN());
		        textField3.setText(String.format("%d",bookChoose.getGenre()));
		        textField4.setText(String.format("%d",bookChoose.getIdPublisher()));
		        textField5.setText(bookChoose.getSummary());
		        textField6.setText(String.format("%d",bookChoose.getLanguage()));
		        textField7.setText(bookChoose.getEdition());
		        textField8.setText(bookChoose.getEdition());
		        textField9.setText(String.format("%s",bookChoose.getPublicationDate()));
		        textField7.setText(String.format("%d",bookChoose.getIdWriter()));
		        textField8.setText(String.format("%d",bookChoose.getQuantity()));
		        textField9.setText(bookChoose.getPosition());
		    }
		});
		

		textField1 = new JTextField();
		textField1.setColumns(20);
		textField1.setEditable(false);

		textField2 = new JTextField();
		textField2.setColumns(20);
		textField2.setEditable(false);

		textField3 = new JTextField();
		textField3.setColumns(20);
		textField3.setEditable(false);

		textField4 = new JTextField();
		textField4.setColumns(20);
		textField4.setEditable(false);

		textField5 = new JTextField();
		textField5.setColumns(20);
		textField5.setEditable(false);

		textField6 = new JTextField();
		textField6.setColumns(20);
		textField6.setEditable(false);

		textField7 = new JTextField();
		textField7.setColumns(20);
		textField7.setEditable(false);

		textField8 = new JTextField();
		textField8.setColumns(20);
		textField8.setEditable(false);
		
		textField9 = new JTextField();
		textField9.setColumns(20);
		textField9.setEditable(false);
		
		textField10 = new JTextField();
		textField10.setColumns(20);
		textField10.setEditable(false);

		textField11 = new JTextField();
		textField11.setColumns(20);
		textField11.setEditable(false);
	
		textSearch = new JTextField();
		textSearch.addKeyListener(new KeyListener(){
			@Override
			public void keyTyped(KeyEvent e) {
			    	  if(ControlFunction.isNumber(textSearch.getText())){  
			    	  }
			    	  else{
			    		  textSearch.setText("");
			    	  }
			}

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				 if(ControlFunction.isNumber(textSearch.getText())){
					 
		    	  }
		    	  else{
		    		  textSearch.setText("");
		    	  }
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				 if(ControlFunction.isNumber(textSearch.getText())){
		    	  }
		    	  else{
		    		  textSearch.setText("");
		    	  }
			}});
		
		textSearch.setColumns(20);
		textSearch.setEditable(true);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			AdminSuccess.main(new String[]{});
			frame.dispose();
			}
		});
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(20)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
						.addComponent(lblIdBook, GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE)
						.addComponent(lblSearch, GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE)
						.addComponent(lblTitle, GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE)
						.addComponent(lblISBN, GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE)
						.addComponent(lblGenre, GroupLayout.DEFAULT_SIZE, 62, GroupLayout.DEFAULT_SIZE)
						.addComponent(lblIdPublisher, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(lblSummary, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.DEFAULT_SIZE)
						.addComponent(lblLanguage, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(lblEdition, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.DEFAULT_SIZE)
						.addComponent(lblPublicationDate, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(lblIdWriter, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.DEFAULT_SIZE)
						.addComponent(lblQuantity, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(lblPosition, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))

					.addGap(58)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
							.addComponent(textSearch, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
							.addComponent(textField11, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
							.addComponent(textField10, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
							.addComponent(textField9, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
							.addComponent(textField8, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
							.addComponent(textField7, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
							.addComponent(textField6, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
							.addComponent(textField5, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
							.addComponent(textField4, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
							.addComponent(textField3, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
							.addComponent(textField2, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
							.addComponent(textField1, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
							.addComponent(listIdBook, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE))
					.addContainerGap(107, Short.MAX_VALUE))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap(151, Short.MAX_VALUE)
					.addComponent(lblAddBook)
					.addGap(144))
				
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap(200, Short.MAX_VALUE)
					.addComponent(btnBack)
					.addGap(169))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(lblAddBook)
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(lblIdBook)
							.addGap(18)
							.addComponent(lblSearch)
							.addGap(18)
							.addComponent(lblTitle))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(listIdBook, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(textSearch, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(textField1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblISBN)
						.addComponent(textField2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblGenre)
						.addComponent(textField3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblIdPublisher)
						.addComponent(textField4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblSummary)
						.addComponent(textField5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblLanguage)
						.addComponent(textField6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblEdition)
						.addComponent(textField7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)	
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblPublicationDate)
						.addComponent(textField8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblIdWriter)
						.addComponent(textField9, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblQuantity)
						.addComponent(textField10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
							.addComponent(lblPosition)
							.addComponent(textField11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					
						.addPreferredGap(ComponentPlacement.RELATED, 57, Short.MAX_VALUE)
						.addComponent(btnBack)
					.addGap(19))
		);
		contentPane.setLayout(gl_contentPane);
	}

}
