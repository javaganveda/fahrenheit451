package it.ganv.fahrenheit451.gui;

import java.awt.EventQueue;

import javax.swing.JFrame;

public class UpdateUserGui extends JFrame {
	static UpdateUserGui frame;

	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new UpdateUserGui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
