package it.ganv.fahrenheit451.gui;

import java.awt.EventQueue;

import javax.swing.JFrame;

public class DeleteUserGui extends JFrame {
static DeleteUserGui frame;

	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new DeleteUserGui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
