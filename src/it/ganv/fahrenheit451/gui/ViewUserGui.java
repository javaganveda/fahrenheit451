package it.ganv.fahrenheit451.gui;

import java.awt.EventQueue;

import javax.swing.JFrame;

public class ViewUserGui extends JFrame {

	static ViewUserGui frame;

	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new ViewUserGui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
}
