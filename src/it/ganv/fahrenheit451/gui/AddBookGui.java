package it.ganv.fahrenheit451.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import javax.swing.JComboBox;

import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.Date;
import java.awt.event.ActionEvent;

import it.ganv.fahrenheit451.domain.Author;
import it.ganv.fahrenheit451.domain.Book;
import it.ganv.fahrenheit451.domain.Publisher;
import it.ganv.fahrenheit451.domain.User;
import it.ganv.fahrenheit451.genericfunction.ControlFunction;
import it.ganv.fahrenheit451.genericfunction.SwingCalendar;
import it.ganv.fahrenheit451.repository.MySqlAuthorRepository;
import it.ganv.fahrenheit451.repository.MySqlBookRepository;
import it.ganv.fahrenheit451.repository.MySqlPublisherRepository;
import it.ganv.fahrenheit451.repository.MySqlUserRepository;
import it.ganv.fahrenheit451.security.AdminLogin;
import it.ganv.fahrenheit451.security.AdminSuccess;

public class AddBookGui extends JFrame {
	static AddBookGui frame;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField1;
	private JTextField textField2;
	private JTextField textField3;
	private JTextField textField5;
	private JTextField textField6;
	private JTextField textField7;
	private JTextField textField8;
	private JTextField textField9;
	private JTextField textField10;
	private JTextField textField11;
	private JComboBox listAuthor;
	private JComboBox listPublisher;

	private JComboBox COMBO;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new AddBookGui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddBookGui() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblAddBook = new JLabel("Add Book");
		lblAddBook.setForeground(Color.DARK_GRAY);
		lblAddBook.setFont(new Font("Tahoma", Font.PLAIN, 22));
		
		JLabel lblIdBook = new JLabel("ID Book:");

		JLabel lblTitle = new JLabel("Title:");
		
		JLabel lblISBN = new JLabel("ISBN:");
		
		JLabel lblGenre = new JLabel("Genre:");
		
		JLabel lblIdPublisher = new JLabel("Id Publisher:");
		
		JLabel lblSummary = new JLabel("Summary:");
		
		JLabel lblLanguage = new JLabel("Language:");
		
		JLabel lblEdition = new JLabel("Edition:");
		
		JLabel lblPublicationDate = new JLabel("Publication Date:");
		
		JLabel lblIdWriter = new JLabel("Id Writer:");
		
		JLabel lblQuantity = new JLabel("Quantity:");
		
		JLabel lblPosition = new JLabel("Position:");
		
		textField = new JTextField();
		textField.setColumns(20);
		textField.setEditable(false);

		textField1 = new JTextField();
		textField1.setColumns(20);
		
		textField2 = new JTextField();
		textField2.setColumns(20);
		
		textField3 = new JTextField();
		textField3.setColumns(20);
		
		
		listPublisher = new JComboBox();
		//textField1.setColumns(20);
		for(Publisher publisher : MySqlPublisherRepository.getAll()) {
			listPublisher.addItem(publisher.getSurname());
		}
		listPublisher.setEditable(false);
		
		textField5 = new JTextField();
		textField5.setColumns(20);
		
		textField6 = new JTextField();
		textField6.setColumns(20);
		
		textField7 = new JTextField();
		textField7.setColumns(20);
		
		textField8 = new JTextField();
		textField8.addKeyListener(new KeyListener(){
			@Override
			public void keyTyped(KeyEvent e) {
			    	  if(ControlFunction.isNumber(textField8.getText())){  
			    	  }
			    	  else{
			    		  textField8.setText("");
			    	  }
			}

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				 if(ControlFunction.isNumber(textField8.getText())){
					 
		    	  }
		    	  else{
		    		  textField8.setText("");
		    	  }
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				 if(ControlFunction.isNumber(textField8.getText())){
		    	  }
		    	  else{
		    		  textField8.setText("");
		    	  }
			}});
		       
		textField8.setColumns(20);
		
		listAuthor = new JComboBox();
		//textField1.setColumns(20);
		for(Author author : MySqlAuthorRepository.getAll()) {
			listAuthor.addItem(author.getSurname());
		}
		listAuthor.setEditable(false);
		
		textField9 = new JTextField();
		textField9.setColumns(20);
		
		textField10 = new JTextField();
		textField10.addKeyListener(new KeyListener(){
			@Override
			public void keyTyped(KeyEvent e) {
			    	  if(ControlFunction.isNumber(textField10.getText())){  
			    	  }
			    	  else{
			    		  textField10.setText("");
			    	  }
			}

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				 if(ControlFunction.isNumber(textField10.getText())){
					 
		    	  }
		    	  else{
		    		  textField10.setText("");
		    	  }
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				 if(ControlFunction.isNumber(textField10.getText())){
		    	  }
		    	  else{
		    		  textField10.setText("");
		    	  }
			}});
		textField10.setColumns(20);

		/*
		textField10.addFocusListener(new FocusListener() {
		      public void focusGained(FocusEvent e) {
		    	  if(ControlFunction.isNumber(textField10.getText())){
		    		  
		    	  }
		    	  else{
		    		  textField10.setText("");
		    	  }
		      }

		        public void focusLost(FocusEvent e) {
		         // displayMessage("Focus lost", e);
		        	 if(ControlFunction.isNumber(textField10.getText())){
			    		  
			    	  }
			    	  else{
			    		  textField10.setText("");
			    	  }
		        }

		      });
		      
		textField10.setColumns(20);*/
		
		
		//textField10.getDocument().addDocumentListener(null);
		
		textField11 = new JTextField();
		textField11.setColumns(20);
	
		
		JButton btnNewButton = new JButton("Add Book");
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

			Book book = new Book();
			
			book.setTitle(textField.getText());
			book.setTitle(textField1.getText());
			book.setISBN(textField2.getText());
			book.setGenre(Integer.parseInt(textField3.getText()));
			book.setIdPublisher((int)listPublisher.getSelectedItem());
			book.setSummary(textField5.getText());
			book.setLanguage(Integer.parseInt(textField6.getText()));
			book.setEdition(textField7.getText());
			book.setPublicationDate(Date.valueOf(textField8.getText()));
			book.setIdWriter(Integer.parseInt(textField9.getText()));
			book.setQuantity(Integer.parseInt(textField10.getText()));
			book.setPosition(textField11.getText());
			
			MySqlBookRepository.addBook(book);
			int i = 1;
			
			if(i>0){
				JOptionPane.showMessageDialog(AddBookGui.this,"Book added successfully!");
				AdminSuccess.main(new String[]{});
				frame.dispose();
				
			}else{
				JOptionPane.showMessageDialog(AddBookGui.this,"Sorry, unable to save!");
			}
			
			
			}
		});
		btnNewButton.setForeground(Color.DARK_GRAY);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			AdminSuccess.main(new String[]{});
			frame.dispose();
			}
		});
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(20)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
						.addComponent(lblIdBook, GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE)
						.addComponent(lblTitle, GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE)
						.addComponent(lblISBN, GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE)
						.addComponent(lblGenre, GroupLayout.DEFAULT_SIZE, 62, GroupLayout.DEFAULT_SIZE)
						.addComponent(lblIdPublisher, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(lblSummary, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.DEFAULT_SIZE)
						.addComponent(lblLanguage, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(lblEdition, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.DEFAULT_SIZE)
						.addComponent(lblPublicationDate, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(lblIdWriter, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.DEFAULT_SIZE)
						.addComponent(lblQuantity, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(lblPosition, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))

					.addGap(58)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
							.addComponent(textField11, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
							.addComponent(textField10, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
							.addComponent(listAuthor, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
							.addComponent(textField8, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
							.addComponent(textField7, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
							.addComponent(textField6, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
							.addComponent(textField5, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
							.addComponent(listPublisher, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
							.addComponent(textField3, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
							.addComponent(textField2, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
							.addComponent(textField1, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
							.addComponent(textField, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE))
					.addContainerGap(107, Short.MAX_VALUE))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap(151, Short.MAX_VALUE)
					.addComponent(lblAddBook)
					.addGap(144))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap(160, Short.MAX_VALUE)
					.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 131, GroupLayout.PREFERRED_SIZE)
					.addGap(133))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap(200, Short.MAX_VALUE)
					.addComponent(btnBack)
					.addGap(169))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(lblAddBook)
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(lblIdBook)
							.addGap(18)
							.addComponent(lblTitle))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(textField1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblISBN)
						.addComponent(textField2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblGenre)
						.addComponent(textField3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblIdPublisher)
						.addComponent(listPublisher, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblSummary)
						.addComponent(textField5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblLanguage)
						.addComponent(textField6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblEdition)
						.addComponent(textField7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)	
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblPublicationDate)
						.addComponent(textField8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblIdWriter)
						.addComponent(listAuthor, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)	
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(textField7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					

					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblQuantity)
						.addComponent(textField10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
							.addComponent(lblPosition)
							.addComponent(textField11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					
					.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED, 57, Short.MAX_VALUE)
						.addComponent(btnBack)
					.addGap(19))
		);
		contentPane.setLayout(gl_contentPane);
	}

}
