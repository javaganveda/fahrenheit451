package it.ganv.fahrenheit451.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import it.ganv.fahrenheit451.Setting;
import it.ganv.fahrenheit451.domain.Publisher;

public class MySqlPublisherRepository {

	private static final String URL = Setting.URL;
	private static final String USER = Setting.USER;
	private static final String PASSWORD = Setting.PASSWORD;
	public static void addPublisher(Publisher publisher) {
		String sql;
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = String.format(Locale.ENGLISH, "INSERT INTO publisher(`name`, `surname`, `biography`, `deleted`) " + 
					"VALUES ('%s','%s','%s', %b)",
					publisher.getName(), publisher.getSurname(),publisher.getBiography(),
					publisher.getBirth(),publisher.isDeleted());
			
			//System.out.println(sql);
			statement.executeUpdate(sql);
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}//FINE ADD
	
	public static void updatePublisher(Publisher publisher,int idPublisher) {
		String sql;

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = String.format(Locale.ENGLISH, "UPDATE `fahrenheit451`.`publisher` SET "
					+ "`name` = '%s',"
					+ "`surname` = '%s',"
					+ "`biography` = '%s',"
					+ "`birth` = '%s',"
					+ "`deleted` = %d"
					+ "WHERE `idpublisher` = %d;",
					
					publisher.getName(),publisher.getSurname(),publisher.getBiography(), publisher.getBirth(),
					publisher.isDeleted(),idPublisher);
			

			System.out.println(sql);
 
			statement.executeUpdate(sql);
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}//FINE UPDATE
	
	public static void deletePublisher(int idPublisher) {
		String sql;
		//UPDATE `campaniart`.`artwork` SET `deleted` = 1 WHERE `id` = 'gio';
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = String.format(Locale.ENGLISH, "UPDATE `fahrenheit451`.`publisher` SET `deleted` = %d "
					+ "WHERE `idpublisher` = %d;",
					1,idPublisher);
					 System.out.println(sql);
			statement.executeUpdate(sql);
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}//FINE DELETE
	
	
	


	public static List<Publisher> getAll() {
		List<Publisher> publisherList = new ArrayList<Publisher>();

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM publisher");

			while (resultSet.next()) {
				Publisher publisher = new Publisher();
				publisher.setIdPublisher(resultSet.getInt("idpublisher"));
				publisher.setName(resultSet.getString("name"));
				publisher.setSurname(resultSet.getString("surname"));
				publisher.setBiography(resultSet.getString("biography"));
				publisher.setBirth(resultSet.getDate("birth"));
				publisher.setDeleted(resultSet.getBoolean("deleted"));
				publisherList.add(publisher);
			}

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return publisherList;
	}//fine get all
	
	
	public static List<Publisher> getAllId() {
		List<Publisher> BookTitleList = new ArrayList<Publisher>();

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM publisher");

			while (resultSet.next()) {
				Publisher books = new Publisher();
				books.setName(resultSet.getString("name"));
				BookTitleList.add(books);
			}

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return BookTitleList;
	}//fine get all
	
	public static Publisher getById(int id) {
		String sql;
		Publisher publisher = new Publisher();

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = String.format("SELECT * FROM publisher c WHERE c.idpublisher = %d", id);
			
			ResultSet resultSet = statement.executeQuery(sql);
			
			if (resultSet.first()) {
				publisher.setIdPublisher(resultSet.getInt("idpublisher"));
				publisher.setName(resultSet.getString("name"));
				publisher.setSurname(resultSet.getString("surname"));
				publisher.setBiography(resultSet.getString("biography"));
				publisher.setBirth(resultSet.getDate("birth"));
				publisher.setDeleted(resultSet.getBoolean("deleted"));
				
			}
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return publisher;
	}
	
	
}//fine classe


