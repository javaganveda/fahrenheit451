package it.ganv.fahrenheit451.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import it.ganv.fahrenheit451.Setting;
import it.ganv.fahrenheit451.domain.Book;
import it.ganv.fahrenheit451.domain.User;

public class MySqlUserRepository {

	private static final String URL = Setting.URL;
	private static final String USER = Setting.USER;
	private static final String PASSWORD = Setting.PASSWORD;
	
	public static void addUser(User user) {
		String sql;
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = String.format(Locale.ENGLISH, "INSERT INTO user(iduser, name, surname, address, cf, telephone,mail,username,password, tipology, deleted) " + 
					"VALUES (%d,  '%s', '%s','%s','%s',%d, '%s','%s','%s',%d,%b)",
					user.getIdUser(),user.getName(), user.getSurname(),user.getAddress(),user.getCF(),user.getTelephone(),
					user.getMail(),user.getUsername(),user.getPassword(),user.getTipology(),user.isDeleted());
			
			
			statement.executeUpdate(sql);
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}//FINE ADD
	
	public static void updateUser(User user,int idUser) {
		String sql;

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = String.format(Locale.ENGLISH, "UPDATE `fahrenheit451`.`user` SET "
					+ "`name` = '%s',"
					+ "`surname` = '%s',"
					+ "`address` = '%s',"
					+ "`cf` = '%s',"
					+ "`telephone` = %d,"
					+ "`mail` = '%s',"
					+ "`username` = '%s',"
					+ "`password` = '%s',"
					+ "`tipology` = %d,"
					+ "`deleted` = %b"
					+ "WHERE `iduser` = %d;",
					
					user.getName(),user.getSurname(),user.getAddress(),user.getCF(), user.getTelephone(),user.getMail(),
					user.getUsername(),user.getPassword(),user.getTipology(),user.isDeleted(),idUser);
			
			/*UPDATE `campaniart`.`artwork` SET `name` = 'gggg',`age` = 4,`city` = 'frr',`address` = 'rf',`civicnumber` = 4,`xsat` = 4.000000,`ysat` = 4.000000,`idartist` = 'yyh' ,`value` = 4.000000,`tipology` = 1,`height` = 2.000000,`width` = 2.000000,`weight` = 2.000000,`lenght` = 2.000000,`description` = 'ggh',`idphoto` = 'ede' WHERE `id` = 'gio';

*/
			System.out.println(sql);
 
			statement.executeUpdate(sql);
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}//FINE UPDATE
	
	public static void deleteUser(int idUser) {
		String sql;
		//UPDATE `campaniart`.`artwork` SET `deleted` = 1 WHERE `id` = 'gio';
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = String.format(Locale.ENGLISH, "UPDATE `fahrenheit451`.`user` SET `deleted` = %d "
					+ "WHERE `iduser` = %d;",
					1,idUser);
					 System.out.println(sql);
			//statement.executeUpdate(sql);
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}//FINE DELETE
	
	
	
	public static List<User> getAll() {
		List<User> userList = new ArrayList<User>();

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM user");

			while (resultSet.next()) {
				User user = new User();
				user.setIdUser(resultSet.getInt("iduser"));
				user.setName(resultSet.getString("name"));
				user.setSurname(resultSet.getString("surname"));
				user.setAddress(resultSet.getString("address"));
				user.setCF(resultSet.getString("cf"));
				user.setTelephone(resultSet.getInt("telephone"));
				user.setMail(resultSet.getString("mail"));
				user.setUsername(resultSet.getString("username"));
				user.setPassword(resultSet.getString("password"));
				user.setDeleted(resultSet.getBoolean("deleted"));

				userList.add(user);
			}

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return userList;
	}//fine get all
	
	
	public static List<User> getAllId() {
		List<User> userList = new ArrayList<User>();

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM user");

			while (resultSet.next()) {
				User users = new User();
				users.setUsername(resultSet.getString("username"));
				userList.add(users);
			}

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return userList;
	}//fine get all
	
	public static User getById(int idUser) {
		String sql;
		User users = new User();

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = String.format("SELECT * FROM user c WHERE c.iduser = %d", idUser);
			
			ResultSet resultSet = statement.executeQuery(sql);
			
			if (resultSet.first()) {
				users.setIdUser(resultSet.getInt("iduser"));
				users.setName(resultSet.getString("name"));
				users.setSurname(resultSet.getString("surname"));
				users.setAddress(resultSet.getString("address"));
				users.setCF(resultSet.getString("cf"));
				users.setTelephone(resultSet.getInt("telephone"));
				users.setMail(resultSet.getString("mail"));
				users.setUsername(resultSet.getString("username"));
				users.setPassword(resultSet.getString("password"));
				users.setDeleted(resultSet.getBoolean("deleted"));
				
			}
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return users;
	}
	
	
}//fine classe


