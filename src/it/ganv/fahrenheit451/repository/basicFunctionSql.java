package it.ganv.fahrenheit451.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import it.ganv.fahrenheit451.Setting;

public class basicFunctionSql {

	private static final String URL = Setting.URL;
	private static final String USER = Setting.USER;
	private static final String PASSWORD = Setting.PASSWORD;
	
	public static void createBookTable(){
		String sql;
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = "CREATE TABLE `fahrenheit451`.`book`(`idbook`INT NOT NULL AUTO_INCREMENT,`title` VARCHAR(45),`isbn` VARCHAR(20),`genre` INT NULL,`idpublisher` INT NULL,"
					+ "`summary` VARCHAR(200) NULL,"
					+ "`language` VARCHAR(20) NULL,"
					+ "`edition` VARCHAR(20) NULL,"
					+ "`publicationdate` VARCHAR(20) NULL,"
					+ "`idwriter` INT NULL,"
					+ "`quantity` INT NULL,"
					+ "`position` VARCHAR(100) NULL,"
					+ "`deleted` TINYINT,"
					+ "PRIMARY KEY (`idbook`))";

			statement.executeUpdate(sql);
		
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
			}
	}//fine create table
	
	
	public static void createUserTable(){
		String sql;
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = "CREATE TABLE `fahrenheit451`.`user`(`iduser`INT NOT NULL AUTO_INCREMENT,`idbook` INT,`name` VARCHAR(20),`surname` VARCHAR(20),`address` VARCHAR(20),`telephone` INT NULL,`mail` VARCHAR(30),`username` VARCHAR(20),`password` VARCHAR(20),`tipology` INT NOT NULL,"
					+ "`deleted` TINYINT,"
					+ "PRIMARY KEY (`iduser`),"
					+ "FOREIGN KEY (`idbook`) REFERENCES book(`idbook`));";

			statement.executeUpdate(sql);
		
	

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
			}
	}//fine create table
	
	
	public static void createPublisherTable(){
		String sql;
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = "CREATE TABLE `fahrenheit451`.`publisher`(`idpublisher`INT NOT NULL AUTO_INCREMENT,`idbook` INT,`name` VARCHAR(20),`surname` VARCHAR(20),`biography` VARCHAR(200),`birth` DATE,"
					+ "`deleted` TINYINT,"
					+ "PRIMARY KEY (`idpublisher`),"
					+ "FOREIGN KEY (`idbook`) REFERENCES book(`idbook`));";

			statement.executeUpdate(sql);
		
	

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
			}
	}//fine create table
	
	public static void createAuthorTable(){
		String sql;
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = "CREATE TABLE `fahrenheit451`.`author`(`idauthor`INT NOT NULL AUTO_INCREMENT,`idbook` INT,`name` VARCHAR(20),`surname` VARCHAR(20),`biography` VARCHAR(200),`birth` DATE,"
					+ "`deleted` TINYINT,"
					+ "PRIMARY KEY (`idauthor`),"
					+ "FOREIGN KEY (`idbook`) REFERENCES book(`idbook`));";

			statement.executeUpdate(sql);
		
	

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
			}
	}//fine create table
	
	public static void createBookingTable(){
		String sql;
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = "CREATE TABLE `fahrenheit451`.`booking`(`idbooking`INT NOT NULL AUTO_INCREMENT,`idbook` INT,`iduser` INT,`reservationdate` DATE,`takingdate` DATE,`deliverydate` DATE,`taken` BOOLEAN,`delivered` BOOLEAN,"
					+ "`deleted` TINYINT,"
					+ "PRIMARY KEY (`idbooking`),"
					+ "FOREIGN KEY (`idbook`) REFERENCES book(`idbook`),"
					+ "FOREIGN KEY (`iduser`) REFERENCES user(`iduser`));";

			statement.executeUpdate(sql);
		
	

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
			}
	}//fine create table
	
}
