package it.ganv.fahrenheit451.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import it.ganv.fahrenheit451.Setting;
import it.ganv.fahrenheit451.domain.Author;

public class MySqlAuthorRepository {

	private static final String URL = Setting.URL;
	private static final String USER = Setting.USER;
	private static final String PASSWORD = Setting.PASSWORD;
	
	public static void addAuthor(Author author) {
		String sql;
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = String.format(Locale.ENGLISH, "INSERT INTO author( name, surname, biography, birth, deleted) " + 
					"VALUES ('%s', '%s','%s','%s', %b)",
					author.getName(), author.getSurname(),author.getBiography(),author.getBirth(),
					author.isDeleted());
			
			statement.executeUpdate(sql);
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}//FINE ADD
	
	public static void updateAuthor(Author author,int idAuthor) {
		String sql;

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = String.format(Locale.ENGLISH, "UPDATE `fahrenheit451`.`author` SET "
					+ "`name` = '%s',"
					+ "`surname` = '%s',"
					+ "`biography` = '%s',"
					+ "`birth` = '%s',"
					+ "`deleted` = %d"
					+ "WHERE `idauthor` = %d;",
					
					author.getName(),author.getSurname(),author.getBiography(),
					author.isDeleted(),idAuthor);
			
			System.out.println(sql);
 
			statement.executeUpdate(sql);
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}//FINE UPDATE
	
	public static void deleteAuthor(int idAuthor) {
		String sql;

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = String.format(Locale.ENGLISH, "UPDATE `fahrenheit451`.`author` SET `deleted` = %d "
					+ "WHERE `idAuthor` = %d;",
					1,idAuthor);
					 System.out.println(sql);
			statement.executeUpdate(sql);
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}//FINE DELETE
	
	
	

	public static List<Author> getAll() {
		List<Author> authorList = new ArrayList<Author>();

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM author");

			while (resultSet.next()) {
				Author author = new Author();
				author.setIdAuthor(resultSet.getInt("idauthor"));
				author.setName(resultSet.getString("name"));
				author.setSurname(resultSet.getString("surname"));
				author.setBiography(resultSet.getString("biography"));
				author.setBirth(resultSet.getDate("birth"));
				author.setDeleted(resultSet.getBoolean("deleted"));
				authorList.add(author);
			}

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return authorList;
	}//fine get all
	
	
	public static List<Author> getAllId() {
		List<Author> authorTitleList = new ArrayList<Author>();

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM author");

			while (resultSet.next()) {
				Author authors = new Author();
				authors.setName(resultSet.getString("name"));
				authorTitleList.add(authors);
			}

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return authorTitleList;
	}//fine get all
	
	public static Author getById(int idAuthor) {
		String sql;
		Author author = new Author();

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = String.format("SELECT * FROM author c WHERE c.idauthor = %d", idAuthor);
			
			ResultSet resultSet = statement.executeQuery(sql);
			
			if (resultSet.first()) {
				author.setIdAuthor(resultSet.getInt("idauthor"));
				author.setName(resultSet.getString("name"));
				author.setSurname(resultSet.getString("surname"));
				author.setBiography(resultSet.getString("biography"));
				author.setBirth(resultSet.getDate("birth"));
				author.setDeleted(resultSet.getBoolean("deleted"));
			}
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return author;
	}
	
	
}//fine classe


