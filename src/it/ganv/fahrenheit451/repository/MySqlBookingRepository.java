package it.ganv.fahrenheit451.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import it.ganv.fahrenheit451.Setting;
import it.ganv.fahrenheit451.domain.Book;
import it.ganv.fahrenheit451.domain.Booking;

public class MySqlBookingRepository {

	private static final String URL = Setting.URL;
	private static final String USER = Setting.USER;
	private static final String PASSWORD = Setting.PASSWORD;
	
	public static void addBooking(Booking booking) {
		String sql;
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = String.format(Locale.ENGLISH, "INSERT INTO booking(idbook, iduser, reservationDate, takingDate, deliveryDate,taken,delivered, deleted) " + 
					"VALUES (%d,%d,'%s', '%s','%s',%b,%b, %b)",
					booking.getIdBook(), booking.getIdUser(), booking.getReservationDate(),booking.getTakingDate(),booking.getDeliveryDate(),
					booking.isTaken(),booking.isDelivered(),
					booking.isDeleted());
			
			//System.out.println(sql);
			statement.executeUpdate(sql);
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}//FINE ADD
	
	public static void updateBooking(Booking booking,int idBooking) {
		String sql;

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = String.format(Locale.ENGLISH, "UPDATE `fahrenheit451`.`booking` SET ,"
					+ "`idBook` = %d,"
					+ "`idUser` = %d,"
					+ "`reservationDate` = %d,"
					+ "`takingDate` = %b,"
					+ "`deliveryDate` = '%d',"
					+ "`taken` = %b,"
					+ "`Delivered` = %b,"
					+ "`deleted` = %b,"
					+ " WHERE `idbooking` = %d;",
					
					booking.getIdBook(),booking.getIdUser(), booking.getReservationDate(),booking.getTakingDate(),booking.getDeliveryDate(),
					booking.isTaken(),booking.isDelivered(),booking.isDeleted(),idBooking);
			
			/*UPDATE `campaniart`.`artwork` SET `name` = 'gggg',`age` = 4,`city` = 'frr',`address` = 'rf',`civicnumber` = 4,`xsat` = 4.000000,`ysat` = 4.000000,`idartist` = 'yyh' ,`value` = 4.000000,`tipology` = 1,`height` = 2.000000,`width` = 2.000000,`weight` = 2.000000,`lenght` = 2.000000,`description` = 'ggh',`idphoto` = 'ede' WHERE `id` = 'gio';

*/
			System.out.println(sql);
 
			statement.executeUpdate(sql);
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}//FINE UPDATE
	
	public static void deleteBooking(int idBooking) {
		String sql;
		//UPDATE `campaniart`.`artwork` SET `deleted` = 1 WHERE `id` = 'gio';
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = String.format(Locale.ENGLISH, "UPDATE `fahrenheit451`.`booking` SET `deleted` = %d "
					+ "WHERE `idbooking` = %d;",
					1,idBooking);
					 System.out.println(sql);
			statement.executeUpdate(sql);
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}//FINE DELETE
	
	
	public static void createSchema(){
		String sql;
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = "CREATE SCHEMA `fahrenheit451` ";
			statement.executeUpdate(sql);

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
			}
	}//fine create schema
	
	public static void createTable(){
		String sql;
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = "CREATE TABLE `fahrenheit451`.`booking`(`idbooking`INT NOT NULL AUTO_INCREMENT,`title` VARCHAR(45),`isbn` VARCHAR(20),`genre` INT NULL,`idpublisher` INT NULL,"
					+ "`summary` VARCHAR(200) NULL,"
					+ "`language` VARCHAR(20) NULL,"
					+ "`edition` VARCHAR(20) NULL,"
					+ "`publicationdate` VARCHAR(20) NULL,"
					+ "`idwriter` INT NULL,"
					+ "`quantity` INT NULL,"
					+ "`position` VARCHAR(100) NULL,"
					+ "`deleted` TINYINT,"
					+ "PRIMARY KEY (`idbooking`))";

			statement.executeUpdate(sql);
		
	

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
			}
	}//fine create table
	

	public static List<Booking> getAll() {
		List<Booking> bookingList = new ArrayList<Booking>();

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM booking");

			while (resultSet.next()) {
				Booking booking = new Booking();
				booking.setIdBooking(resultSet.getInt("idbooking"));
				booking.setIdBook(resultSet.getInt("idbook"));
				booking.setIdUser(resultSet.getInt("iduser"));
				booking.setReservationDate(resultSet.getDate("ReservationDate"));
				booking.setTakingDate(resultSet.getDate("TakingDate"));
				booking.setDeliveryDate(resultSet.getDate("DeliveryDate"));
				booking.setTaken(resultSet.getBoolean("Taken"));
				booking.setDelivered(resultSet.getBoolean("delivered"));
				booking.setDeleted(resultSet.getBoolean("deleted"));
				

				bookingList.add(booking);
			}

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return bookingList;
	}//fine get all
	

	public static Booking getById(int id) {
		String sql;
		Booking booking = new Booking();

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = String.format("SELECT * FROM booking c WHERE c.id = %d", id);
			
			ResultSet resultSet = statement.executeQuery(sql);
			
			if (resultSet.first()) {
				booking.setIdBooking(resultSet.getInt("idbooking"));
				booking.setIdBook(resultSet.getInt("idbook"));
				booking.setIdUser(resultSet.getInt("user"));
				booking.setReservationDate(resultSet.getDate("ReservationDate"));
				booking.setTakingDate(resultSet.getDate("TakingDate"));
				booking.setDeliveryDate(resultSet.getDate("DeliveryDate"));
				booking.setTaken(resultSet.getBoolean("Taken"));
				booking.setDelivered(resultSet.getBoolean("delivered"));
				booking.setDeleted(resultSet.getBoolean("deleted"));
				
			}
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return booking;
	}
	
	public static List<Booking> getAllId() {
		List<Booking> BookingTitleList = new ArrayList<Booking>();

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM booking");

			while (resultSet.next()) {
				Booking bookings = new Booking();
				bookings.setIdBooking(resultSet.getInt("idbooking"));
				BookingTitleList.add(bookings);
			}

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return BookingTitleList;
	}//fine get all
	
	
}//fine classe


