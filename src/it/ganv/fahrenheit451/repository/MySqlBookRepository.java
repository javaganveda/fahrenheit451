package it.ganv.fahrenheit451.repository;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import it.ganv.fahrenheit451.Setting;
import it.ganv.fahrenheit451.domain.*;

public class MySqlBookRepository {

	private static final String URL = Setting.URL;
	private static final String USER = Setting.USER;
	private static final String PASSWORD = Setting.PASSWORD;
	
		public static void addBook(Book book) {
			String sql;
			try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
				Statement statement = connection.createStatement();
				sql = String.format(Locale.ENGLISH, "INSERT INTO book(title, isbn, genre, idpublisher, summary,language,edition,publicationdate, idwriter, quantity, position, deleted) " + 
						"VALUES ('%s', '%s', %d,%d,'%s', '%s','%s','%s',%d,%d,'%s',%b)",
						book.getTitle(),book.getISBN(), book.getGenre(),book.getIdPublisher(),book.getSummary(),
						book.getLanguage(),book.getEdition(),book.getPublicationDate(),book.getIdWriter(),book.getQuantity(), book.getPosition(),
						book.isDeleted());
				
				
				statement.executeUpdate(sql);
				
			} catch (SQLException e) {
				System.out.println("SQL exception");
				e.printStackTrace();
			}
		}//FINE ADD
		
		public static void updateBook(Book book,int idBook) {
			String sql;

			try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
				Statement statement = connection.createStatement();
				sql = String.format(Locale.ENGLISH, "UPDATE `fahrenheit451`.`book` SET `title` = '%s',"
						+ "`isbn` = '%s',"
						+ "`genre` = %s,"
						+ "`idpublisher` = %d,"
						+ "`summary` = %d,"
						+ "`language` = '%s',"
						+ "`edition` = '%s',"
						+ "`publicationdate` = '%s',"
						+ "`idwriter` = %d,"
						+ "`quantity` = %d,"
						+ "`position` = '%s',"
						+ "`deleted` = %b"
						+ " WHERE `idbook` = %d;",
						
						book.getTitle(),book.getISBN(), book.getGenre(),book.getIdPublisher(),book.getSummary(),
						book.getLanguage(),book.getEdition(),book.getPublicationDate(),book.getIdWriter(),book.getQuantity(), book.getPosition(),
						book.isDeleted(),idBook);
				
				/*UPDATE `campaniart`.`artwork` SET `name` = 'gggg',`age` = 4,`city` = 'frr',`address` = 'rf',`civicnumber` = 4,`xsat` = 4.000000,`ysat` = 4.000000,`idartist` = 'yyh' ,`value` = 4.000000,`tipology` = 1,`height` = 2.000000,`width` = 2.000000,`weight` = 2.000000,`lenght` = 2.000000,`description` = 'ggh',`idphoto` = 'ede' WHERE `id` = 'gio';

	*/
				//System.out.println(sql);
	 
				statement.executeUpdate(sql);
				
			} catch (SQLException e) {
				System.out.println("SQL exception");
				e.printStackTrace();
			}
		}//FINE UPDATE
		
		public static void deleteBook(int idBook) {
			String sql;
			//UPDATE `campaniart`.`artwork` SET `deleted` = 1 WHERE `id` = 'gio';
			try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
				Statement statement = connection.createStatement();
				sql = String.format(Locale.ENGLISH, "UPDATE `fahrenheit451`.`book` SET `deleted` = %d "
						+ "WHERE `idbook` = %d;",
						1,idBook);
				//		 System.out.println(sql);
				statement.executeUpdate(sql);
				
			} catch (SQLException e) {
				System.out.println("SQL exception");
				e.printStackTrace();
			}
		}//FINE DELETE
		
		
		public static void createSchema(){
			String sql;
			try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
				Statement statement = connection.createStatement();
				sql = "CREATE SCHEMA `fahrenheit451` ";
				statement.executeUpdate(sql);

			} catch (SQLException e) {
				System.out.println("SQL exception");
				e.printStackTrace();
				}
		}//fine create schema
		
		public static void createTable(){
			String sql;
			try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
				Statement statement = connection.createStatement();
				sql = "CREATE TABLE `fahrenheit451`.`book`(`idbook`INT NOT NULL AUTO_INCREMENT,`title` VARCHAR(45),`isbn` VARCHAR(20),`genre` VARCHAR(20),`idpublisher` INT NULL,"
						+ "`summary` VARCHAR(200) NULL,"
						+ "`language` VARCHAR(20) NULL,"
						+ "`edition` VARCHAR(20) NULL,"
						+ "`publicationdate` VARCHAR(20) NULL,"
						+ "`idwriter` INT NULL,"
						+ "`quantity` INT NULL,"
						+ "`position` VARCHAR(100) NULL,"
						+ "`deleted` TINYINT,"
						+ "PRIMARY KEY (`idbook`))";

				statement.executeUpdate(sql);
			
		

			} catch (SQLException e) {
				System.out.println("SQL exception");
				e.printStackTrace();
				}
		}//fine create table
		

		public static List<Book> getAll() {
			List<Book> bookList = new ArrayList<Book>();

			try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
				Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery("SELECT * FROM book");

				while (resultSet.next()) {
					Book book = new Book();
					book.setIdBook(resultSet.getInt("idbook"));
					book.setTitle(resultSet.getString("title"));
					book.setISBN(resultSet.getString("isbn"));
					book.setGenre(resultSet.getInt("Genre"));
					book.setIdPublisher(resultSet.getInt("idpublisher"));
					book.setSummary(resultSet.getString("summary"));
					book.setLanguage(resultSet.getInt("language"));
					book.setEdition(resultSet.getString("edition"));
					book.setPublicationDate(resultSet.getDate("publicationdate"));
					book.setIdWriter(resultSet.getInt("idwriter"));
					book.setQuantity(resultSet.getInt("quantity"));
					book.setPosition(resultSet.getString("position"));
					book.setDeleted(resultSet.getBoolean("deleted"));


					bookList.add(book);
				}

			} catch (SQLException e) {
				System.out.println("SQL exception");
				e.printStackTrace();
			}

			return bookList;
		}//fine get all
		
		
		public static List<Book> getAllId() {
			List<Book> BookTitleList = new ArrayList<Book>();

			try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
				Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery("SELECT * FROM book");

				while (resultSet.next()) {
					Book books = new Book();
					books.setTitle(resultSet.getString("title"));
					BookTitleList.add(books);
				}

			} catch (SQLException e) {
				System.out.println("SQL exception");
				e.printStackTrace();
			}

			return BookTitleList;
		}//fine get all
		
		public static Book getById(int id) {
			String sql;
			Book books = new Book();

			try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
				Statement statement = connection.createStatement();
				sql = String.format("SELECT * FROM book c WHERE c.idbook = %d", id);
				
				ResultSet resultSet = statement.executeQuery(sql);
				
				if (resultSet.first()) {
					books.setIdBook(resultSet.getInt("idbook"));
					books.setTitle(resultSet.getString("title"));
					books.setISBN(resultSet.getString("isbn"));
					books.setGenre(resultSet.getInt("genre"));
					books.setIdPublisher(resultSet.getInt("idpublisher"));
					books.setSummary(resultSet.getString("summary"));
					books.setLanguage(resultSet.getInt("language"));
					books.setEdition(resultSet.getString("edition"));
					books.setPublicationDate(resultSet.getDate("publicationdate"));
					books.setIdWriter(resultSet.getInt("idwriter"));
					books.setQuantity(resultSet.getInt("quantity"));
					books.setPosition(resultSet.getString("position"));
					books.setDeleted(resultSet.getBoolean("deleted"));
					
				}
				
			} catch (SQLException e) {
				System.out.println("SQL exception");
				e.printStackTrace();
			}

			return books;
		}
		
		
	}//fine classe


