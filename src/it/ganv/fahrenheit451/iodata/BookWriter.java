package it.ganv.fahrenheit451.iodata;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import it.ganv.fahrenheit451.domain.Book;

public class BookWriter {

	private static final String COMMA_SEPARATOR = ",";
	private static final String NEW_LINE_SEPARATOR = "\n";

	public static void saveBook(List<Book> books) {
		BufferedWriter bufferedWriter = null;

		try {
			 bufferedWriter = new BufferedWriter(new FileWriter("heroes.csv"));
			 
			for (Book book : books) {
				/* bufferedWriter.append(String.format("%s%s%d%s%s", company.getName(), COMMA_SEPARATOR,
															  company.getCode(), COMMA_SEPARATOR,
															  company.getCity(), COMMA_SEPARATOR)); */
				bufferedWriter.append(book.getIdBook() + COMMA_SEPARATOR +
						book.getTitle() + COMMA_SEPARATOR +
						book.getISBN()+ COMMA_SEPARATOR + book.getGenre() 
										+ COMMA_SEPARATOR + book.getIdPublisher() + COMMA_SEPARATOR +
										book.getSummary() + COMMA_SEPARATOR + book.getLanguage()
										+ COMMA_SEPARATOR + book.getEdition() + COMMA_SEPARATOR +
										book.getPublicationDate()+ COMMA_SEPARATOR + book.getIdWriter() +
										book.getQuantity() + COMMA_SEPARATOR + book.getPosition()
										+ book.isDeleted());
				
				bufferedWriter.append(NEW_LINE_SEPARATOR);
			}
				System.out.println("File salvato correttamente");
		} catch (IOException e) {
			System.out.println("Error to write on file");

		} finally {
			try {
				bufferedWriter.close();

			} catch (IOException e) {
				System.out.println("Error to close file");
			}
		}
	}
}
