package it.ganv.fahrenheit451.iodata;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import it.ganv.fahrenheit451.domain.Book;

public class BookReader {

	static int[] elementType = new int[5];
	private static final String COMMA_SEPARATOR = ",";
	
	public static List<Book> getHeroes(){
		List<Book> book = new ArrayList<Book>();

		String line = "";

		try (BufferedReader bufferedReader = new BufferedReader(new FileReader("book.csv"))) {
			while ((line = bufferedReader.readLine()) != null) {
				String[] tokens = line.split(COMMA_SEPARATOR);
			
			
				Book books = new Book();
				books.setIdBook(Integer.valueOf(tokens[0]));
				books.setTitle(String.valueOf(tokens[1]));
				books.setISBN(String.valueOf(tokens[2]));
				books.setGenre(Integer.valueOf(tokens[3]));
				books.setIdPublisher(Integer.valueOf(tokens[4]));
				books.setSummary(String.valueOf(tokens[5]));
				books.setLanguage(Integer.valueOf(tokens[6]));
				books.setEdition(String.valueOf(tokens[7]));
				books.setPublicationDate(Date.valueOf(tokens[8]));
				books.setIdWriter(Integer.valueOf(tokens[9]));
				books.setQuantity(Integer.valueOf(tokens[10]));
				books.setPosition(String.valueOf(tokens[11]));
				books.setDeleted(Boolean.valueOf(tokens[12]));
				book.add(books);

			}

		} catch (Exception e) {
			System.out.println("Error to read file");
			e.printStackTrace();
		}

		return book;
	}
}
