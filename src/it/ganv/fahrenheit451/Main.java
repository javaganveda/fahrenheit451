package it.ganv.fahrenheit451;

import java.io.IOException;

import it.ganv.fahrenheit451.booking.TimeControlBooking;
import it.ganv.fahrenheit451.genericfunction.InputFunction;
import it.ganv.fahrenheit451.gui.GraphicMenu;
import it.ganv.fahrenheit451.gui.TextualMenu;
import it.ganv.fahrenheit451.repository.MySqlAuthorRepository;
import it.ganv.fahrenheit451.repository.MySqlBookingRepository;
import it.ganv.fahrenheit451.repository.MySqlUserRepository;

public class Main {

	public static void main(String Arg[])
	{
		System.out.println("Benvenuto...");

		System.out.println(TimeControlBooking.GetCurrentDateTime());

		GraphicMenu.main(Arg);
//		TextualMenu.startMenu();
		System.out.println("A presto...");

	}

}
