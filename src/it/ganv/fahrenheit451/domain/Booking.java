package it.ganv.fahrenheit451.domain;
import java.util.Date;

public class Booking {

	private int idBooking;
	private int idBook;
	private int idUser;
	private Date reservationDate;
	private Date takingDate;
	private Date deliveryDate;
	private boolean taken;
	private boolean delivered;
	private boolean deleted;

	
	
	public Booking() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Booking(int idBooking, int idBook, int idUser, Date reservationDate, Date takingDate, Date deliveryDate,
			boolean taken, boolean delivered, boolean deleted) {
		super();
		this.idBooking = idBooking;
		this.idBook = idBook;
		this.idUser = idUser;
		this.reservationDate = reservationDate;
		this.takingDate = takingDate;
		this.deliveryDate = deliveryDate;
		this.taken = taken;
		this.delivered = delivered;
		this.deleted = deleted;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}


	public int getIdBooking() {
		return idBooking;
	}
	public void setIdBooking(int idBooking) {
		this.idBooking = idBooking;
	}
	public int getIdBook() {
		return idBook;
	}
	public void setIdBook(int idBook) {
		this.idBook = idBook;
	}
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public Date getReservationDate() {
		return reservationDate;
	}
	public void setReservationDate(Date reservationDate) {
		this.reservationDate = reservationDate;
	}
	public Date getTakingDate() {
		return takingDate;
	}
	public void setTakingDate(Date takingDate) {
		this.takingDate = takingDate;
	}
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	
	public void setTaken(boolean taken) {
		this.taken = taken;
	}

	public boolean isDelivered() {
		return delivered;
	}

	public void setDelivered(boolean delivered) {
		this.delivered = delivered;
	}

	public boolean isTaken() {
		return taken;
	}


}
