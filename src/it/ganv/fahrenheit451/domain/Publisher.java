package it.ganv.fahrenheit451.domain;
import java.util.Date;

public class Publisher {

	private int idPublisher;
	private String name;
	private String surname;
	private String biography;
	private Date birth;
	private boolean deleted;
	
	
	
	public Publisher() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	public Publisher(int idPublisher, int idBook, String name, String surname, String biography, Date birth,
			boolean deleted) {
		super();
		this.idPublisher = idPublisher;
		this.name = name;
		this.surname = surname;
		this.biography = biography;
		this.birth = birth;
		this.deleted = deleted;
	}
	

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	
	public int getIdPublisher() {
		return idPublisher;
	}
	public void setIdPublisher(int idPublisher) {
		this.idPublisher = idPublisher;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getBiography() {
		return biography;
	}
	public void setBiography(String biography) {
		this.biography = biography;
	}
	public Date getBirth() {
		return birth;
	}
	public void setBirth(Date birth) {
		this.birth = birth;
	}
	
	
}
