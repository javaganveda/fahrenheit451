package it.ganv.fahrenheit451.domain;

public class User {
	
	private int idUser;
	private String name;
	private String surname;
	private String address;
	private int telephone;
	private String mail;
	private String username;
	private String password;
	private int tipology;
	private String CF;
	private boolean deleted;
	
	



	public User() {
		super();
	}

	
	public User(int idUser, String name, String surname, String address, int telephone, String mail,
			String username, String password, int tipology, String cf, boolean deleted) {
		super();
		this.idUser = idUser;
		this.name = name;
		this.surname = surname;
		this.address = address;
		this.telephone = telephone;
		this.mail = mail;
		this.username = username;
		this.password = password;
		this.tipology = tipology;
		this.deleted = deleted;
		this.CF = cf;
	}
	
	public String getCF() {
		return CF;
	}


	public void setCF(String cF) {
		CF = cF;
	}
	
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getTelephone() {
		return telephone;
	}
	public void setTelephone(int telephone) {
		this.telephone = telephone;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getTipology() {
		return tipology;
	}
	public void setTipology(int tipology) {
		this.tipology = tipology;
	}

	
	
}
