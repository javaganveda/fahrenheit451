package it.ganv.fahrenheit451.domain;
import java.util.Date;


public class Book {

	private int idBook;
	private String title;
	private String ISBN;
	private int genre;
	private int idPublisher;
	private String summary;
	private int language;
	private String edition;
	private Date publicationDate;
	private int idWriter;
	private int quantity;
	private String position;
	private boolean deleted;
	
	
	public Book() {
		super();
	}

	public Book(int idBook, String title, String iSBN, int genre, int idPublisher, String summary, int language,
			String edition, Date publicationDate, int idWriter, int quantity, String position) {
		super();
		this.idBook = idBook;
		this.title = title;
		this.ISBN = iSBN;
		this.genre = genre;
		this.idPublisher = idPublisher;
		this.summary = summary;
		this.language = language;
		this.edition = edition;
		this.publicationDate = publicationDate;
		this.idWriter = idWriter;
		this.quantity = quantity;
		this.position = position;
		
	}
	
	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	
	public int getIdBook() {
		return idBook;
	}
	public void setIdBook(int idBook) {
		this.idBook = idBook;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getISBN() {
		return ISBN;
	}
	public void setISBN(String iSBN) {
		this.ISBN = iSBN;
	}
	public int getGenre() {
		return genre;
	}
	public void setGenre(int keyInt) {
		this.genre = keyInt;
	}
	public int getIdPublisher() {
		return idPublisher;
	}
	public void setIdPublisher(int idPublisher) {
		this.idPublisher = idPublisher;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public int getLanguage() {
		return language;
	}
	public void setLanguage(int language) {
		this.language = language;
	}
	public String getEdition() {
		return edition;
	}
	public void setEdition(String edition) {
		this.edition = edition;
	}
	public Date getPublicationDate() {
		return publicationDate;
	}
	public void setPublicationDate(Date publicationDate) {
		this.publicationDate = publicationDate;
	}
	public int getIdWriter() {
		return idWriter;
	}
	public void setIdWriter(int idWriter) {
		this.idWriter = idWriter;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	
}
