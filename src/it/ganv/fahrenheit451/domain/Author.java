package it.ganv.fahrenheit451.domain;
import java.sql.Date;

public class Author {

	private int idAuthor;
	private String name;
	private String surname;
	private String biography;
	private Date birth;
	private boolean deleted;

	public Author() {
		super();
	}

	public Author(int idAuthor, String name, String surname, String biography, Date birth) {
		super();
		this.idAuthor = idAuthor;
		this.name = name;
		this.surname = surname;
		this.biography = biography;
		this.birth = birth;
	}
	
	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	
	public int getIdAuthor() {
		return idAuthor;
	}
	public void setIdAuthor(int idAuthor) {
		this.idAuthor = idAuthor;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getBiography() {
		return biography;
	}
	public void setBiography(String biography) {
		this.biography = biography;
	}
	public Date getBirth() {
		return birth;
	}
	public void setBirth(Date birth) {
		this.birth = birth;
	}
	
	

	
}
