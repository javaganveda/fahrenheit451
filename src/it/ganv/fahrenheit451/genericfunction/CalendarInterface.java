package it.ganv.fahrenheit451.genericfunction;

import javax.swing.JPanel;
import javax.swing.JTextField;

public interface CalendarInterface {
	
	public void handleInsertedDate(String data, SwingCalendar panelGlobal);

}
