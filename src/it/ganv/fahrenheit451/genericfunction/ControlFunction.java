package it.ganv.fahrenheit451.genericfunction;

public class ControlFunction {

	
	 public static boolean isNumber(String textInsert){
	  
		  try {
			    int d = Integer.parseInt(textInsert); 
			    // or Integer.parseInt(text), etc.
			    // OK, valid number.
				 return true;

			  } catch (NumberFormatException nfe) {
			    // Not a number.
					 return false;

			  }
	 }
}
