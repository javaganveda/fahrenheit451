package it.ganv.fahrenheit451.genericfunction;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;

public class SwingCalendar extends JFrame {
	DefaultTableModel model;
	Calendar cal = new GregorianCalendar();
	JLabel label;
	public static int positionxCalendar = 0;
	public static int positionyCalendar = 0;
	String month = null;
	int year = 0;
	int cont = 0;
	int mounth2 = 0;
	private CalendarInterface ie;
//	private CalendarInterface callback;
	SwingCalendar panelGlobal = null;

	public SwingCalendar(int x, int y,CalendarInterface callback) {

		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		this.setTitle("Calandar");
		this.setSize(300,200);
		this.setLayout(new BorderLayout());
		this.setLocation(x, y);
		this.setVisible(true);
		ie=callback;

		label = new JLabel();
		label.setHorizontalAlignment(SwingConstants.CENTER);

		JButton b1 = new JButton("<-");
		b1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				cal.add(Calendar.MONTH, -1);
				cont--;

				//mounth2 = mounth2 -1;
				updateMonth();
			}
		});

		JButton b2 = new JButton("->");
		b2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				cal.add(Calendar.MONTH, + 1);
				cont++;
				updateMonth();

			}
		});
		
		panelGlobal = this;
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.add(b1,BorderLayout.WEST);
		panel.add(label,BorderLayout.CENTER);
		panel.add(b2,BorderLayout.EAST);

		
		String [] columns = {"Sun","Mon","Tue","Wed","Thu","Fri","Sat"};
		model = new DefaultTableModel(null,columns);
		JTable table = new JTable(model);
		JScrollPane panelTable = new JScrollPane(table);
		table.setRowSelectionAllowed( false );
		table.setColumnSelectionAllowed( false );
		table.setCellSelectionEnabled( false );
		table.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e)
			{
				String data= null;
	
				int row=table.rowAtPoint(e.getPoint());
				int col= table.columnAtPoint(e.getPoint());

				int day = (int) table.getValueAt(row, col);
				mounth2 = Calendar.MONTH + cont;
				
				if(mounth2<1)
					mounth2=12;
				if(mounth2>12)
					mounth2=1;
				if(mounth2<10){
					if(day<10){
						data = String.format("%d-0%d-0%d", year,mounth2,day);
								}
					else{
						data = String.format("%d-0%d-%d", year,mounth2,day);
						}
				}
				else
				{
					data = String.format("%d-%d-%d", year,mounth2,day);
				}
				if (ie != null) {
				//	panel.setVisible(false);
				ie.handleInsertedDate(data,panelGlobal);
				panelGlobal.setVisible(false);

				}

				System.out.println(data);
			}});

		this.add(panel,BorderLayout.NORTH);
		this.add(panelTable,BorderLayout.CENTER);

		this.updateMonth();
	}


	void updateMonth() {
		cal.set(Calendar.DAY_OF_MONTH, 1);
		month = cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.US);
		//mounth2 = Calendar.MONTH;

		year = cal.get(Calendar.YEAR);
		label.setText(month + " " + year);

		int startDay = cal.get(Calendar.DAY_OF_WEEK);
		int numberOfDays = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		int weeks = cal.getActualMaximum(Calendar.WEEK_OF_MONTH);

		model.setRowCount(0);
		model.setRowCount(weeks);

		int i = startDay-1;
		for(int day=1;day<=numberOfDays;day++){
			model.setValueAt(day, i/7 , i%7 );    
			i = i + 1;
		}

	}

	public void openCalendar(CalendarInterface callback) {

		JFrame.setDefaultLookAndFeelDecorated(true);
		
		
//		this.callback = callback;

	//	SwingCalendar sc = new SwingCalendar(callback);
	}

}