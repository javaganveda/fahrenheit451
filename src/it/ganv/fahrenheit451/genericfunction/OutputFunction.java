package it.ganv.fahrenheit451.genericfunction;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import it.ganv.fahrenheit451.Setting;
import it.ganv.fahrenheit451.domain.Book;
import it.ganv.fahrenheit451.domain.Booking;

public class OutputFunction {

	private static final String URL = Setting.URL;
	private static final String USER = Setting.USER;
	private static final String PASSWORD = Setting.PASSWORD;
	
	private static final String URL() {
		return null;
	}
	
	private static final String USER() {
		return null;
	}
	private static final String PASSWORD() {
		return null;
	}
	
	
	public void print(){
		
	}
		public static List<Booking> getAllId() {
			List<Booking> BookingTitleList = new ArrayList<Booking>();

			try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD))
					{
				Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery("SELECT * FROM booking");
			
				while (resultSet.next()) {
					Booking bookings = new Booking();
					bookings.setIdBooking(resultSet.getInt("idbooking"));
					BookingTitleList.add(bookings);
					}

			} catch (SQLException e) {
				System.out.println("SQL exception");
				e.printStackTrace();
			}

			return BookingTitleList;
		}
	
}
