package it.ganv.fahrenheit451.genericfunction;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import it.ganv.fahrenheit451.domain.Author;
import it.ganv.fahrenheit451.domain.Book;
import it.ganv.fahrenheit451.domain.Booking;
import it.ganv.fahrenheit451.domain.Publisher;
import it.ganv.fahrenheit451.domain.User;
import it.ganv.fahrenheit451.repository.MySqlPublisherRepository;
import it.ganv.fahrenheit451.security.Encrypted;

public class InputFunction {
	
	public final static char CR  = (char) 0x0D;
	
	public static Book insertBook(){
		String keyString;
		int keyInt;

		Book book = new Book();
		
		
		try (Scanner keyPressed = new Scanner(System.in))
		{
		//inizio inserimento
		System.out.println("Insert a new book");
		System.out.println("Insert name:");
		book.setTitle(controlInputString(keyPressed,20));


		System.out.println("Insert ISBN:");
		book.setISBN(controlInputString(keyPressed,20));
		
		System.out.println("Insert genre:");
		book.setGenre(controlInputInt(keyPressed));
		
		//insert IDPUBBLISHERcontrolInputString(keyPressed,20)
		
		System.out.println("Insert publisher:");
		SearchFunction.chooseIdPubliscer();
		book.setIdPublisher(controlInputInt(keyPressed));
		keyString = keyPressed.nextLine();

		//////////
		System.out.println("Insert summary:");
		book.setSummary(controlInputString(keyPressed,200));
		
		System.out.println("Insert language:");
		keyInt = keyPressed.nextInt();
		book.setLanguage(keyInt);
		
		keyString = keyPressed.nextLine();

		System.out.println("Insert edition:");
		book.setEdition(controlInputString(keyPressed,20));
		
		System.out.println("Insert Publication Date:");
		//keyString = keyPressed.nextLine();
		book.setPublicationDate(controlInputdate(keyPressed));	

		System.out.println("Insert writer:");
		SearchFunction.chooseIdWriter();
		keyInt = keyPressed.nextInt();
		book.setIdWriter(keyInt);

		System.out.println("Insert quantity:");
		keyInt = keyPressed.nextInt();
		book.setQuantity(keyInt);			
		keyString = keyPressed.nextLine();

		System.out.println("Insert Position:");
		book.setPosition(controlInputString(keyPressed,20));

		book.setDeleted(false);
		
		return book;	
		}
	}
	
	public static Publisher insertPublisher(){
		Scanner keyPressed = new Scanner(System.in);
		String keyString;
		int keyNumberInt;
		double keyNumberDecimal;

		Publisher publisher = new Publisher();
		
		//inizio inserimento
		System.out.println("Insert a new publisher");
		System.out.println("Insert name:");
		keyString = keyPressed.nextLine();
		publisher.setName(keyString);

		System.out.println("Insert surname:");
		keyString = keyPressed.nextLine();
		publisher.setSurname(keyString);
		
		System.out.println("Insert biography:");
		keyString = keyPressed.nextLine();
		publisher.setBiography(keyString);
		
		System.out.println("Insert birthdate:(yyyy-mm-gg)");
		keyString = keyPressed.nextLine();
		publisher.setBirth(Date.valueOf(keyString));
		
		publisher.setDeleted(false);
		
		return publisher;
	}	
	
	public static Author insertAuthor(){
		Scanner keyPressed = new Scanner(System.in);
		String keyString;
		int keyNumberInt;
		double keyNumberDecimal;

		Author author = new Author();
		
		//inizio inserimento
		System.out.println("Insert a new author");
		System.out.println("Insert name:");
		keyString = keyPressed.nextLine();
		author.setName(keyString);

		System.out.println("Insert surname:");
		keyString = keyPressed.nextLine();
		author.setSurname(keyString);
		
		System.out.println("Insert biography:");
		keyString = keyPressed.nextLine();
		author.setBiography(keyString);
		
		System.out.println("Insert birthdate:(yyyy-mm-gg)");
		keyString = keyPressed.nextLine();
		author.setBirth(controlInputdate(keyPressed));
		
		author.setDeleted(false);
		
		return author;
	}	
	
	public static User insertUser(){
		String keyString;
		int keyInt;

		User user = new User();
		
		
		try (Scanner keyPressed = new Scanner(System.in))
		{
		//inizio inserimento
		System.out.println("Insert a new user");
		System.out.println("Insert name:");
		keyString = keyPressed.nextLine();
		user.setName(keyString);

		System.out.println("Insert surname:");
		keyString = keyPressed.nextLine();
		user.setSurname(keyString);
		
		System.out.println("Insert address:");
		keyString = keyPressed.nextLine();
		user.setAddress(keyString);
		
		
		System.out.println("Insert Telephone:");
		keyInt = keyPressed.nextInt();
		user.setTelephone(keyInt);
		keyString = keyPressed.nextLine();

		System.out.println("Insert mail:");
		keyString = keyPressed.nextLine();
		user.setMail(keyString);
		
		System.out.println("Insert username:");
		keyString = keyPressed.nextLine();
		user.setUsername(keyString);
		
		System.out.println("Insert password:");
		keyString = keyPressed.nextLine();
		
		try {
			user.setPassword(Encrypted.md5(keyString));
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		System.out.println("Insert typology (0 - simple user - 1 administrator:");
		keyInt = keyPressed.nextInt();
		user.setTipology(keyInt);
		
		keyString = keyPressed.nextLine();
		System.out.println("Insert CF:");
		keyString = keyPressed.nextLine();
		user.setCF(keyString);
		
		user.setDeleted(false);
		//keyPressed.close();
		return user;	
		}
	}
	
	
	public static Booking insertBooking(){
		String keyString;
		int keyInt;

		Booking booking = new Booking();
		
		
		try (Scanner keyPressed = new Scanner(System.in))
		{
		//inizio inserimento
		System.out.println("Insert a new booking");
		
		
		System.out.println("Insert Book:");
		SearchFunction.chooseIdBook();
		keyInt = keyPressed.nextInt();
		booking.setIdBook(keyInt);

		System.out.println("Insert User:");
		SearchFunction.chooseIdUser();
		keyInt = keyPressed.nextInt();
		booking.setIdUser(keyInt);
		keyString = keyPressed.nextLine();

		System.out.println("Do you want to book a book? (Y/n)");
		keyString = keyPressed.nextLine();
		if(keyString == "Y"){
			Calendar today = Calendar.getInstance();
			today.set(Calendar.HOUR_OF_DAY, 0);
			booking.setReservationDate(Date.valueOf("2100-11-11"));
			booking.setTakingDate(Date.valueOf("2100-11-11"));
			booking.setDeliveryDate(Date.valueOf("2100-11-11"));
			booking.setTaken(false);
			booking.setDelivered(false);
		}
		else
		{
			Calendar today = Calendar.getInstance();
			today.set(Calendar.HOUR_OF_DAY, 0);
			booking.setTakingDate(Date.valueOf("2000-12-12"));
			booking.setReservationDate(Date.valueOf("2000-12-12"));
			booking.setDeliveryDate(Date.valueOf("2000-12-12"));
			booking.setTaken(true);
			booking.setDelivered(true);
		}
	
		
		booking.setDeleted(false);
		
		return booking;	
		}
	}
	
	public static User insertLoginUser(){
		String keyString;
		int keyInt;

		User user = new User();
		
		
		try (Scanner keyPressed = new Scanner(System.in))
		{
		System.out.println("Insert username:");
		keyString = keyPressed.nextLine();
		user.setUsername(keyString);
		
		System.out.println("Insert password:");
		keyString = keyPressed.nextLine();
		
		try {
			user.setPassword(Encrypted.md5(keyString));
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return user;	
		}
	}
	
	public static String controlInputString(Scanner keyPressed,int maxDimInput){
		
		String inputObject= null,inputObject2 = null;
		
		while(inputObject == null || inputObject==""){
		
		inputObject = keyPressed.nextLine();
		if(inputObject.length()>maxDimInput ||inputObject.length()<2)
		{
			System.out.println("Lunghezza massima superata o dato non inserito. Inserisci correttamente il dato");
			inputObject= null;
		}
		else
		{
			inputObject2=inputObject;
		}
		}
		
		return inputObject2;
	}
	
	public static int controlInputInt(Scanner keyPressed){
	
		String keyPressedString = null;
		int inputObject2 = 0;
		
		
		while(keyPressedString == null){

		keyPressedString = keyPressed.next();
		
		
		if(Integer.valueOf(keyPressedString)== null)
		{
			inputObject2 = Integer.valueOf(keyPressedString);
			keyPressedString= null;
		}
		else{
			inputObject2 = Integer.valueOf(keyPressedString);
		}

		}
		return inputObject2;
	}
	
	
	public static Date controlInputdate(Scanner keyPressed){
		
		String dateFormatted = null;
		
		int day,mounth,year;
		
		System.out.println("Indica il giorno");
		day = keyPressed.nextInt();
		System.out.println("Indica il mese");
		mounth = keyPressed.nextInt();
		System.out.println("Indica l'anno");
		year = keyPressed.nextInt();

		return Date.valueOf(String.format(Locale.ENGLISH,"%s-%d-%d",year,mounth,day));
	}
	
	public static String controlkeyboardInput(){
		
		
		return null;
		
	}
	
	public static void cleanInput(Scanner scanner){
		
		if(scanner.hasNextLine()){
			scanner.nextLine();
		}
		if(scanner.hasNextInt()){

		}
		
	}
	
}
