package it.ganv.fahrenheit451.genericfunction;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.stream.Collectors;

import it.ganv.fahrenheit451.domain.Author;
import it.ganv.fahrenheit451.domain.Book;
import it.ganv.fahrenheit451.domain.Booking;
import it.ganv.fahrenheit451.domain.Publisher;
import it.ganv.fahrenheit451.domain.User;
import it.ganv.fahrenheit451.repository.MySqlAuthorRepository;
import it.ganv.fahrenheit451.repository.MySqlBookRepository;
import it.ganv.fahrenheit451.repository.MySqlBookingRepository;
import it.ganv.fahrenheit451.repository.MySqlPublisherRepository;
import it.ganv.fahrenheit451.repository.MySqlUserRepository;

public class SearchFunction {

	public static void chooseIdPubliscer(){
		
		for(Publisher publisher : MySqlPublisherRepository.getAll()) {
			System.out.println("|ID:\t" + publisher.getIdPublisher() + "\n" + "|Name:\t" + publisher.getName() +"\n|"
			+ "----------------------------");
		}
		
	}

	public static void chooseIdWriter(){
		
		for(Author author : MySqlAuthorRepository.getAll()) {
			System.out.println("|ID:\t" + author.getIdAuthor() + "\n" + "|Name:\t" + author.getName() +"\n|"
			+ "----------------------------");
		}
		
	}
	
public static void chooseIdBook(){
		
		for(Book book : MySqlBookRepository.getAll()) {
			if(!(book.isDeleted())){
			System.out.println("|ID:\t" + book.getIdPublisher() + "\n" + "|Name:\t" + book.getTitle() +"\n|"
			+ "----------------------------");
			}
		}
		
	}

public static void chooseIdUser(){
	
	for(User user : MySqlUserRepository.getAll()) {
		if(!(user.isDeleted())){

		System.out.println("|ID:\t" + user.getIdUser() + "\n" + "|Name:\t" + user.getName() +"\n|"
		+ "----------------------------");
	}
	}
	
}

public static void chooseIdPublisher(){
	
	for(Publisher publisher : MySqlPublisherRepository.getAll()) {
		if(!(publisher.isDeleted())){

		System.out.println("|ID:\t" + publisher.getIdPublisher() + "\n" + "|Name:\t" + publisher.getName() +"\n|"
		+ "----------------------------");
	}}
	
}

public static void chooseIdAuthor(){
	
	for(Author author : MySqlAuthorRepository.getAll()) {
		if(!(author.isDeleted())){
		System.out.println("|ID:\t" + author.getIdAuthor() + "\n" + "|Name:\t" + author.getName() +"\n|"
		+ "----------------------------");
	}}
	
}

public static void searchBook(){
	
	String keyString;

	try (Scanner keyPressed = new Scanner(System.in))
	{
	//inizio inserimento
	System.out.println("Insert name of book to search");
	keyString = keyPressed.nextLine();

	for(Book book : MySqlBookRepository.getAll()) {
		
		if(book.getTitle().contains(keyString))
		{
			System.out.println("|ID:\t" + book.getIdPublisher() + "\n" + "|Name:\t" + book.getTitle() +"\n|"
					+ "----------------------------");		}
		}
	
	}

}

public static void searchBooking(){
	
	int keyInt;
	Scanner keyPressed = new Scanner(System.in);

	
	//inizio inserimento
	System.out.println("Insert id of booking to search");
	keyInt = keyPressed.nextInt();

	for(Booking booking : MySqlBookingRepository.getAll()) {
		
		if(booking.getIdBooking() == keyInt)
		{
			System.out.println("|ID:\t" + booking.getIdBooking() + "\n" + "|user:\t" + booking.getIdUser() + "|Book:\t" + booking.getIdBook() + "\n|"
					+ "----------------------------");		}
		}
	
	keyPressed.close();

}


public static void searchUser(){
	
	String keyString;

	try (Scanner keyPressed = new Scanner(System.in))
	{
	//inizio inserimento
	System.out.println("Insert name of book to search");
	keyString = keyPressed.nextLine();

	for(User user : MySqlUserRepository.getAll()) {
		
	//	if(user.getName().equals(keyString))
		if(user.getName().contains(keyString))
		{
			System.out.println("|ID:\t" + user.getIdUser() + "\n" + "|Name:\t" + user.getName() +"\n|"
					+ "----------------------------");		}
		}
	
	}

}

public static void searchPublisher(){
	
	String keyString;

	try (Scanner keyPressed = new Scanner(System.in))
	{
	//inizio inserimento
	System.out.println("Insert name of book to search");
	keyString = keyPressed.nextLine();

	for(Publisher publisher : MySqlPublisherRepository.getAll()) {
		
	//	if(user.getName().equals(keyString))
		if(publisher.getName().contains(keyString))
		{
			System.out.println("|ID:\t" + publisher.getIdPublisher() + "\n" + "|Name:\t" + publisher.getName() +"\n|"
					+ "----------------------------");		}
		}
	
	}

}

public static void searchAuthor(){
	
	String keyString;

	try (Scanner keyPressed = new Scanner(System.in))
	{
	//inizio inserimento
	System.out.println("Insert name of book to search");
	keyString = keyPressed.nextLine();

	for(Author author : MySqlAuthorRepository.getAll()) {
		
	//	if(user.getName().equals(keyString))
		if(author.getName().contains(keyString))
		{
			System.out.println("|ID:\t" + author.getIdAuthor() + "\n" + "|Name:\t" + author.getName() +"\n|"
					+ "----------------------------");		}
		}
	
	}

}

public static boolean controlDuplicated(User usertemp){
	
	boolean duplicated = false;
	
	for(User user : MySqlUserRepository.getAll()) {
		
			if(user.getCF().equals(usertemp.getCF()))
			{
				System.out.println("UTENTE gi� registrato");
				duplicated=true;
			}
			else
			{
				if(user.getUsername().equals(usertemp.getUsername())){
					System.out.println("username non disponibile");
					duplicated=true;
				}
				else
				{
				duplicated=false;
				}
			}
			}
		
	return duplicated;
}

public static void chooseIdBooking(){
	
	for(Booking booking : MySqlBookingRepository.getAll()) {
		System.out.println("|ID:\t" + booking.getIdBooking() + "\n" + "|user:\t" + booking.getIdUser() + "|Book:\t" + booking.getIdBook() + "\n|"
		+ "----------------------------");
	}
	
}

public static void viewStatus(){
	//vedi lo stato delle prenotazioni
}
}
