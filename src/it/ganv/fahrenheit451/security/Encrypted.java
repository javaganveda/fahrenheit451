package it.ganv.fahrenheit451.security;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class Encrypted {

		        public static String md5(String data)
			            throws NoSuchAlgorithmException, UnsupportedEncodingException {
			        // Get the algorithm:
			        MessageDigest md5 = MessageDigest.getInstance("MD5");
			        // Calculate Message Digest as bytes:
			        byte[] digest = md5.digest(data.getBytes("UTF-8"));
			        // Convert to 32-char long String:
			        return String.format("%032x%n", new BigInteger(1, digest));
			    }
}
