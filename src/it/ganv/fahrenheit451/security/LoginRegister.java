package it.ganv.fahrenheit451.security;

import it.ganv.fahrenheit451.Setting;
import it.ganv.fahrenheit451.domain.User;
import it.ganv.fahrenheit451.genericfunction.InputFunction;
import it.ganv.fahrenheit451.genericfunction.SearchFunction;
import it.ganv.fahrenheit451.repository.MySqlUserRepository;

public class LoginRegister {

	public static void Register(){
		
		
		User usertemp = new User();
		usertemp =InputFunction.insertUser();
		if(!SearchFunction.controlDuplicated(usertemp)){		
		MySqlUserRepository.addUser(usertemp);
		}
		
	}
	
	public static void Login(){
		
		User usertemp;
		usertemp = InputFunction.insertLoginUser();
		
		String username = usertemp.getUsername();
		String password = usertemp.getPassword();

		for(User user : MySqlUserRepository.getAll()) {
			
			if(user.getUsername().equals(username))
			{
				System.out.println("Username ok");

				if(user.getPassword().equals(password)){
					
					System.out.println("Accesso Riuscito");
					if(user.getTipology()==0)
					{
						Setting.registerUser=true;
						Setting.simpleUser=false;
						Setting.administrator=false;
					}
					else if(user.getTipology()==1)
						Setting.registerUser=false;
						Setting.simpleUser=false;
						Setting.administrator=true;
					}
				else
				{
					Setting.registerUser=false;
					Setting.simpleUser=true;
					Setting.administrator=false;
				}
			}
		}
	
		
		
	}
	
}
