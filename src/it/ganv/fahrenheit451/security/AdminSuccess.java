package it.ganv.fahrenheit451.security;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import it.ganv.fahrenheit451.gui.AddBookGui;
import it.ganv.fahrenheit451.gui.AddBookingGui;
import it.ganv.fahrenheit451.gui.AddPublisherGui;
import it.ganv.fahrenheit451.gui.AddUserGui;
import it.ganv.fahrenheit451.gui.DeleteBookGui;
import it.ganv.fahrenheit451.gui.DeleteBookingGui;
import it.ganv.fahrenheit451.gui.DeletePublisherGui;
import it.ganv.fahrenheit451.gui.DeleteUserGui;
import it.ganv.fahrenheit451.gui.GraphicMenu;
import it.ganv.fahrenheit451.gui.UpdateBookGui;
import it.ganv.fahrenheit451.gui.UpdateBookingGui;
import it.ganv.fahrenheit451.gui.UpdatePublisherGui;
import it.ganv.fahrenheit451.gui.UpdateUserGui;
import it.ganv.fahrenheit451.gui.ViewBookGui;
import it.ganv.fahrenheit451.gui.ViewBookingGui;
import it.ganv.fahrenheit451.gui.ViewPublisherGui;
import it.ganv.fahrenheit451.gui.ViewUserGui;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.sql.*;

public class AdminSuccess extends JFrame {
	static AdminSuccess frame;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new AdminSuccess();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AdminSuccess() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblAdminSection = new JLabel("Admin");
		lblAdminSection.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblAdminSection.setForeground(Color.GRAY);
		///////BOOK
		JButton btnAddBook = new JButton("Add Book");
		btnAddBook.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnAddBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			AddBookGui.main(new String[]{});
			frame.dispose();
			}
		});
		
		JButton btnViewBook = new JButton("View Book");
		btnViewBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			ViewBookGui.main(new String[]{});
			}
		});
		btnViewBook.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
		JButton btnUpdateBook = new JButton("Update Book");
		btnUpdateBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			UpdateBookGui.main(new String[]{});
			frame.dispose();
			}
		});
		btnUpdateBook.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
		
		JButton btnDeleteBook = new JButton("Delete Book");
		btnDeleteBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			DeleteBookGui.main(new String[]{});
			frame.dispose();
			}
		});
		btnDeleteBook.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
		JButton btnLogout = new JButton("Logout");
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				GraphicMenu.main(new String[]{});
				frame.dispose();
			}
		});
		btnLogout.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
		////USER
		JButton btnAddUser = new JButton("Add User");
		btnAddUser.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnAddUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			AddUserGui.main(new String[]{});
			frame.dispose();
			}
		});
		
		JButton btnViewUser = new JButton("View User");
		btnViewUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			ViewUserGui.main(new String[]{});
			}
		});
		btnViewUser.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
		JButton btnUpdateUser = new JButton("Update User");
		btnUpdateUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			UpdateUserGui.main(new String[]{});
			frame.dispose();
			}
		});
		btnUpdateUser.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
		
		JButton btnDeleteUser = new JButton("Delete User");
		btnDeleteUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			DeleteUserGui.main(new String[]{});
			frame.dispose();
			}
		});
		btnDeleteUser.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
	
	////Publisher
			JButton btnAddPublisher = new JButton("Add Publisher");
			btnAddPublisher.setFont(new Font("Tahoma", Font.PLAIN, 15));
			btnAddPublisher.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
				AddPublisherGui.main(new String[]{});
				frame.dispose();
				}
			});
			
			JButton btnViewPublisher = new JButton("View Publisher");
			btnViewPublisher.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
				ViewPublisherGui.main(new String[]{});
				}
			});
			btnViewPublisher.setFont(new Font("Tahoma", Font.PLAIN, 15));
			
			JButton btnUpdatePublisher = new JButton("Update Publisher");
			btnUpdatePublisher.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
				UpdatePublisherGui.main(new String[]{});
				frame.dispose();
				}
			});
			btnUpdatePublisher.setFont(new Font("Tahoma", Font.PLAIN, 15));
			
			
			JButton btnDeletePublisher = new JButton("Delete Publisher");
			btnDeletePublisher.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
				DeletePublisherGui.main(new String[]{});
				frame.dispose();
				}
			});
			btnDeletePublisher.setFont(new Font("Tahoma", Font.PLAIN, 15));
			
		////Publisher
					JButton btnAddBooking = new JButton("Add Booking");
					btnAddBooking.setFont(new Font("Tahoma", Font.PLAIN, 15));
					btnAddBooking.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
						AddBookingGui.main(new String[]{});
						frame.dispose();
						}
					});
					
					JButton btnViewBooking = new JButton("View Booking");
					btnViewBooking.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent arg0) {
						ViewBookingGui.main(new String[]{});
						}
					});
					btnViewBooking.setFont(new Font("Tahoma", Font.PLAIN, 15));
					
					JButton btnUpdateBooking = new JButton("Update Booking");
					btnUpdateBooking.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
						UpdateBookingGui.main(new String[]{});
						frame.dispose();
						}
					});
					btnUpdateBooking.setFont(new Font("Tahoma", Font.PLAIN, 15));
					
					
					JButton btnDeleteBooking = new JButton("Delete Booking");
					btnDeletePublisher.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
						DeleteBookingGui.main(new String[]{});
						frame.dispose();
						}
					});
					btnDeleteBooking.setFont(new Font("Tahoma", Font.PLAIN, 15));
					
		/// POSIZIONAMENTO PULSANTI
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap(150, Short.MAX_VALUE)
					.addComponent(lblAdminSection, GroupLayout.PREFERRED_SIZE, 151, GroupLayout.PREFERRED_SIZE)
					.addGap(123))
				.addGroup(Alignment.LEADING, gl_contentPane.createSequentialGroup()
					.addGap(134)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(btnLogout, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnDeleteBook, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnUpdateBook, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnViewBook, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnAddBook, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnDeleteUser, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnUpdateUser, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnViewUser, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnAddUser, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnDeletePublisher, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnUpdatePublisher, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnViewPublisher, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnAddPublisher, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnDeleteBooking, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnUpdateBooking, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnViewBooking, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnAddBooking, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(109, Short.MAX_VALUE))
		);
		
		
/*		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(lblAdminSection, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
					.addGap(11)
					.addComponent(btnAddBook, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(btnViewBook, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(btnUpdateBook, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(btnDeleteBook, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
					.addGap(50)
					.addComponent(lblAdminSection, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
					.addGap(11)
					.addComponent(btnAddUser, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(btnViewUser, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(btnUpdateUser, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(btnDeleteUser, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
					.addGap(50)
					.addComponent(btnAddPublisher, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(btnViewPublisher, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(btnUpdatePublisher, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(btnDeletePublisher, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
					.addGap(50)
					.addComponent(btnAddBooking, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(btnViewBooking, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(btnUpdateBooking, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(btnDeleteBooking, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
					.addGap(50)
					.addComponent(btnLogout, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(21, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);*/
		
	}
}
